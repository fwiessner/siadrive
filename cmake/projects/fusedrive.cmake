#SiaDrive FUSE API
if (UNIX)
  file(GLOB_RECURSE SIADRIVE_FUSE_API_SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/src/fuse_api/*.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/fuse_api/*.cxx
    ${CMAKE_CURRENT_SOURCE_DIR}/src/fuse_api/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/include/fuse_api/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/include/fuse_api/*.hpp
    )

  add_library(siadrive.fuse.api SHARED ${SIADRIVE_FUSE_API_SOURCES})
  if (MACOS)
    find_library(OSXFUSE NAMES OSXFUSE)
    if (OSXFUSE-NOTFOUND)
      message(FATAL_ERROR "FUSE for macOS not found (https://osxfuse.github.io/)")
    endif ()
    set(LIBFUSE_LIBRARIES osxfuse)
  endif ()
  target_include_directories(siadrive.fuse.api PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include/fuse_api
    )
  add_dependencies(siadrive.fuse.api siadrive.common.api)
  target_link_libraries(siadrive.fuse.api PRIVATE siadrive.common.api ${LIBFUSE_LIBRARIES} ${COMMON_LIBRARIES})
endif ()