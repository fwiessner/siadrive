file(GLOB_RECURSE SIADRIVED_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/src/siadrived/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/siadrived/*.cxx
  ${CMAKE_CURRENT_SOURCE_DIR}/src/siadrived/*.c
  ${CMAKE_CURRENT_SOURCE_DIR}/include/siadrived/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/include/siadrived/*.hpp
  )

if (MACOS)
  add_executable(siadrived MACOSX_BUNDLE ${SIADRIVED_SOURCES} ${SIADRIVE_PLAT_SOURCES})
  set_target_properties(siadrived PROPERTIES INSTALL_RPATH "@executable_path/..")
  set_target_properties(siadrived PROPERTIES BUILD_WITH_INSTALL_RPATH TRUE)
else()
  add_executable(siadrived ${SIADRIVED_SOURCES} ${SIADRIVE_PLAT_SOURCES})
endif()

target_compile_definitions(siadrived PRIVATE SIADRIVE_PRIVATE_API)
target_include_directories(siadrived PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/include/siadrived
)
add_dependencies(siadrived siadrive.common.api)
if(MSVC)
  add_dependencies(siadrived siadrive.dokany.api)
  target_link_libraries(siadrived PRIVATE siadrive.dokany.api siadrive.common.api ${COMMON_LIBRARIES})
  if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_compile_options(siadrived PRIVATE "/MDd")
  else ()
    target_compile_options(siadrived PRIVATE "/MD")
  endif ()
  CodeSignFile(siadrived siadrived.exe)
else()
  add_dependencies(siadrived siadrive.fuse.api)
  target_link_libraries(siadrived PRIVATE siadrive.fuse.api siadrive.common.api ${COMMON_LIBRARIES} ${Boost_LIBRARIES} ${OPENSSL_LIBRARIES})
endif()
if (MACOS)
  target_link_libraries(siadrived PRIVATE "-framework Foundation" "-framework AppKit")
endif ()
