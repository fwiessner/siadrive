file(GLOB_RECURSE SIADRIVECLI_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/src/siadrivecli/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/siadrivecli/*.cxx
  ${CMAKE_CURRENT_SOURCE_DIR}/src/siadrivecli/*.c
  ${CMAKE_CURRENT_SOURCE_DIR}/include/siadrivecli/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/include/siadrivecli/*.hpp
  )

if (MACOS)
  add_executable(siadrivecli MACOSX_BUNDLE ${SIADRIVECLI_SOURCES} ${SIADRIVE_PLAT_SOURCES})
  set_target_properties(siadrivecli PROPERTIES INSTALL_RPATH "@executable_path/..")
  set_target_properties(siadrivecli PROPERTIES BUILD_WITH_INSTALL_RPATH TRUE)
else()
  add_executable(siadrivecli ${SIADRIVECLI_SOURCES} ${SIADRIVE_PLAT_SOURCES})
endif()

target_include_directories(siadrivecli PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/include/siadrivecli
  )
add_dependencies(siadrivecli siadrive.drive.api siadrive.common.api)

target_link_libraries(siadrivecli PRIVATE siadrive.drive.api siadrive.common.api ${COMMON_LIBRARIES})
if(MSVC)
  if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_compile_options(siadrivecli PRIVATE "/MDd")
  else ()
    target_compile_options(siadrivecli PRIVATE "/MD")
  endif ()
  CodeSignFile(siadrivecli siadrivecli.exe)
endif()
if (MACOS)
  target_link_libraries(siadrivecli PRIVATE "-framework Foundation" "-framework AppKit")
endif ()