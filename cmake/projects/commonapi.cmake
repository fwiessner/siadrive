file(GLOB_RECURSE SIADRIVE_API_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/src/common_api/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/common_api/*.cxx
  ${CMAKE_CURRENT_SOURCE_DIR}/src/common_api/*.c
  ${CMAKE_CURRENT_SOURCE_DIR}/include/common_api/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/include/common_api/*.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/jsonrpcpp-1.0.0/lib/jsonrp.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/jsonrpcpp-1.0.0/lib/jsonrp.hpp
  )
set(SIADRIVE_SYSTEM_INCLUDES ${SIADRIVE_SYSTEM_INCLUDES} ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/jsonrpcpp-1.0.0/lib)

add_library(siadrive.common.api SHARED ${SIADRIVE_API_SOURCES} ${SIADRIVE_PLAT_SOURCES})
if (MSVC)
  target_compile_definitions(siadrive.common.api PRIVATE SIADRIVE_EXPORT_SYMBOLS=1)
  if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_compile_options(siadrive.common.api PRIVATE "/MDd")
  else ()
    target_compile_options(siadrive.common.api PRIVATE "/MD")
  endif ()
  CodeSignFile(siadrive.common.api siadrive.common.api.dll)
endif ()
if (MACOS)
  target_include_directories(siadrive.common.api PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include/platform/${OS_TYPE})
  target_link_libraries(siadrive.common.api PRIVATE "-framework Foundation" "-framework AppKit")
endif ()
if (UNIX)
  add_dependencies(siadrive.common.api cryptopp_project cpprestsdk_project)
endif ()
target_link_libraries(siadrive.common.api PUBLIC
  ${SQLITE_CPP_LIBRARIES}
  ${CURL_LIBRARIES}
  ${CRYPTOPP_LIBRARIES}
  ${CPPRESTSDK_LIBRARIES}
  )
add_dependencies(siadrive.common.api curl_project sqlitecpp_project cpprestsdk_project)
target_link_libraries(siadrive.common.api PRIVATE ${COMMON_LIBRARIES})