#SiaDrive Drive API
file(GLOB_RECURSE SIADRIVE_DRIVE_API_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/src/drive_api/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/drive_api/*.cxx
  ${CMAKE_CURRENT_SOURCE_DIR}/src/drive_api/*.c
  ${CMAKE_CURRENT_SOURCE_DIR}/include/drive_api/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/include/drive_api/*.hpp
)

add_library(siadrive.drive.api SHARED ${SIADRIVE_DRIVE_API_SOURCES})
if (MSVC)
  target_compile_definitions(siadrive.drive.api PRIVATE SIADRIVE_DRIVE_EXPORT_SYMBOLS=1)
  if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_compile_options(siadrive.drive.api PRIVATE "/MDd")
  else ()
    target_compile_options(siadrive.drive.api PRIVATE "/MD")
  endif ()
  CodeSignFile(siadrive.drive.api siadrive.drive.api.dll)
endif ()
target_include_directories(siadrive.drive.api PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/include/drive_api
)
add_dependencies(siadrive.drive.api siadrive.common.api cpprestsdk_project)
target_link_libraries(siadrive.drive.api PRIVATE siadrive.common.api ${CPPRESTSDK_LIBRARIES} ${COMMON_LIBRARIES})
if (MACOS)
  target_link_libraries(siadrive.drive.api PRIVATE "-framework Foundation" "-framework AppKit")
endif ()
