# Binary Dependencies
if (SIADRIVE_DOWNLOAD_DEPS)
  if (MSVC)
    set(VC_REDIST_PATH "${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/vc_redist_${VC_REDIST_VERSION}.x64.exe")
    DownloadFile("https://go.microsoft.com/fwlink/?LinkId=746572" "${VC_REDIST_PATH}")
    
    set(DOKANY_REDIST_PATH "${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/Dokany_${DOKANY_VERSION}_x64.msi")
    DownloadFile("https://github.com/dokan-dev/dokany/releases/download/v${DOKANY_VERSION}/Dokan_x64-${DOKANY_VERSION_FULL}.msi" "${DOKANY_REDIST_PATH}")
  endif ()
  
  set(SIA_NAME Sia-v${SIA_VERSION}-${OS_TYPE}-amd64)
  set(SIA_ZIP_PATH ${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/${SIA_NAME}.zip)
  set(SIA_EXTRACT_PATH ${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/${SIA_NAME})
  set(SIA_DEST_PATH ${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/sia)
  if (NOT EXISTS ${SIA_DEST_PATH})
    file(REMOVE_RECURSE ${SIA_EXTRACT_PATH})
    file(REMOVE "${SIA_ZIP_PATH}")

    DownloadFile("https://github.com/NebulousLabs/Sia/releases/download/v${SIA_VERSION}/${SIA_NAME}.zip" "${SIA_ZIP_PATH}")

    execute_process(COMMAND ${UNZIP_EXE} ${SIA_ZIP_PATH}
      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}
      RESULT_VARIABLE result)
    if (NOT result EQUAL 0)
      file(REMOVE_RECURSE ${SIA_EXTRACT_PATH})
      file(REMOVE_RECURSE ${SIA_DEST_PATH})
      file(REMOVE "${SIA_ZIP_PATH}")
      message(FATAL_ERROR "Extracting ${SIA_NAME}.zip failed")
    else ()
      file(RENAME "${SIA_EXTRACT_PATH}" "${SIA_DEST_PATH}")
    endif ()
  endif ()
endif ()