@echo off
set ROOT=%~dp0%
set CMAKE_BASE_VER=3.9
set CMAKE_VER=cmake-%CMAKE_BASE_VER%.1
set CMAKE=%ROOT%bin\%CMAKE_VER%-win64-x64\bin\cmake.exe
set CTEST=%ROOT%bin\%CMAKE_VER%-win64-x64\bin\ctest.exe
set PATH=%ROOT%bin;%ROOT%bin\%CMAKE_VER%-win64-x64\bin;%PATH%
set TARGET_MODE=%1
set ENABLE_TEST=%2
set ENABLE_CODE_SIGNING=OFF
if "%USERNAME%"=="sgraves" (
  set ENABLE_CODE_SIGNING=OFF
)   
set VS_CMAKE_GENERATOR=Visual Studio 15 2017 Win64

pushd "%ROOT%"

if NOT EXIST "%CMAKE%" (
  for /f %%i in ('dir /a:d /s /b bin\cmake-*') do (
    rd /s /q "%%i"
  )
  cls         
  echo Downloading %CMAKE_VER%-win64-x64
  del /q %CMAKE_VER%-win64-x64.zip >NUL 2>&1
  echo https://cmake.org/files/v%CMAKE_BASE_VER%/%CMAKE_VER%-win64-x64.zip 
  wget --no-check-certificate https://cmake.org/files/v%CMAKE_BASE_VER%/%CMAKE_VER%-win64-x64.zip || goto :ERROR
  echo Extracting %CMAKE_VER%-win64-x64
  unzip -o -q -d bin\ %CMAKE_VER%-win64-x64.zip || goto :ERROR
  del /q %CMAKE_VER%-win64-x64.zip >NUL 2>&1
)
 
if EXIST "dist\%TARGET_MODE%" (
  echo Cleaning previous build [dist\%TARGET_MODE%]
  rd /s /q dist\%TARGET_MODE% >NUL 2>&1
)

mkdir build >NUL 2>&1
mkdir build\%TARGET_MODE% >NUL 2>&1
pushd build\%TARGET_MODE% >NUL 2>&1
  echo Building [%TARGET_MODE%]
  ((%CMAKE% -G"%VS_CMAKE_GENERATOR%" -DSIADRIVE_ENABLE_SIGNING=OFF -DSIADRIVE_DOWNLOAD_DEPS=ON -DCMAKE_BUILD_TYPE=%TARGET_MODE% -DSIADRIVE_INSTALL_FOLDER="%ROOT%dist\%TARGET_MODE%" ..\..) && (%CMAKE% --build . --config %TARGET_MODE%) && (
    %CMAKE% -G"%VS_CMAKE_GENERATOR%" -DSIADRIVE_ENABLE_SIGNING=%ENABLE_CODE_SIGNING% -DSIADRIVE_DOWNLOAD_DEPS=OFF -DCMAKE_BUILD_TYPE=%TARGET_MODE% -DSIADRIVE_INSTALL_FOLDER="%ROOT%dist\%TARGET_MODE%" ..\..) && (%CMAKE% --build . --target install --config %TARGET_MODE%) && (
      if "%ENABLE_TEST%"=="1" (
        (%CTEST% -V -C %TARGET_MODE%) || goto :ERROR
      )
      rd /s /q "%ROOT%dist\%TARGET_MODE%\htdocs\.idea">NUL 2>&1
      if "%TARGET_MODE%"=="Release" (
        del /q "%ROOT%dist\%TARGET_MODE%\*.lib">NUL 2>&1
        del /q "%ROOT%dist\%TARGET_MODE%\*.pdb">NUL 2>&1
      )
    )
  ) || goto :ERROR
popd
goto :END

:ERROR
  popd
  exit 1

:END
  popd