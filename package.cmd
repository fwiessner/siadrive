@echo off

set ROOT=%~dp0%
set PATH=%ROOT%bin;%PATH%
set MODE=%1
set INNO_COMPILER=C:\Program Files (x86)\Inno Setup 5\Compil32.exe
pushd "%ROOT%"     

setlocal
  call build_%MODE%_x64.cmd 1
endlocal
                                            
for /f "tokens=*" %%i in ('type SiaDrive_Packager_%MODE%.iss ^| grep "#define\ MyAppVersion" ^| sed -e "s/#define\ MyAppVersion//g" ^| sed -e "s/\""//g"') do (
  set APP_VER=%%i
) 
del /q %ROOT%dist\SiaDrive_%APP_VER%_%MODE%_x64.sig >NUL 2>&1

"%INNO_COMPILER%" /cc SiaDrive_Packager_%MODE%.iss || goto :ERROR                                               
(certutil -hashfile %ROOT%dist\SiaDrive_%APP_VER%_%MODE%_x64.exe SHA512 | sed -e "1d" -e "$d" -e "s/\ //g") > %ROOT%dist\SiaDrive_%APP_VER%_%MODE%_x64.sig
goto :END

:ERROR
  popd
  exit 1

:END
  popd