#include <platform.h>
#include <Wincrypt.h>
#include <base64.h>
#include <tlhelp32.h>
#include <siadriveconfig.h>
#include <ShlObj.h>
#include <filepath.h>
#include <shlguid.h>
#include <Shlobj.h>

using namespace Sia::Api;

bool Sia::Api::PlatformLaunchBundledSiad(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) {
  FilePath dataPath(FilePath::GetAppDataDirectory(), "siadrive");
  dataPath.Append("data");
  dataPath.MakeDirectory();  
  return PlatformExecuteProcess(siaDriveConfig, FilePath("./sia/") + SIAD_EXECUTABLE, dataPath, false);
}

SString Sia::Api::PlatformRetrieveString(const SString &key) {
  return GetRegistry(key, "", true);
}

void Sia::Api::PlatformStoreString(const SString &key, const SString &data) {
  SetRegistry(key, data);
}

bool Sia::Api::PlatformCheckSingleInstance(const SString &id) {
  bool ret = false;
  auto mtx = ::CreateMutex(nullptr, FALSE, &id[0]);
  if (::WaitForSingleObject(mtx, 5000) == WAIT_OBJECT_0) {
    ret = true;
  } else {
    ::CloseHandle(mtx);
  }
  return ret;
}

void Sia::Api::PlatformReleaseSingleInstance() {
}

SString Sia::Api::PlatformSecureDecryptString(const SString &key, const SString &encryptedData) {
  SString ret;
  std::vector<BYTE> decoded;
  Base64::Decode((BYTE *) &encryptedData[0], encryptedData.Length(), decoded);
  DATA_BLOB inputData = {0};
  inputData.pbData = &decoded[0];
  inputData.cbData = decoded.size();
  DATA_BLOB outputData = {0};
  LPWSTR n = nullptr;
  if (::CryptUnprotectData(&inputData, &n, nullptr, nullptr, nullptr, 0, &outputData)) {
    if (key == n) {
      ret = std::wstring(reinterpret_cast<const wchar_t *>(outputData.pbData), outputData.cbData / sizeof(wchar_t));
    }
  }
  return ret;
}

bool Sia::Api::PlatformIsProcessRunning(const SString &processName) {
  bool exists = false;
  PROCESSENTRY32 entry;
  entry.dwSize = sizeof(PROCESSENTRY32);
  HANDLE snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
  if (::Process32First(snapshot, &entry)) {
    while (not exists && ::Process32Next(snapshot, &entry)) {
      exists = SString(entry.szExeFile).EndsWith(processName);
    }
  }
  ::CloseHandle(snapshot);
  return exists;
}

bool Sia::Api::PlatformIsProcessRunningExactPath(const FilePath &filePath) {
  FilePath absPath(filePath);
  absPath.MakeAbsolute();

  bool exists = false;
  PROCESSENTRY32 entry;
  entry.dwSize = sizeof(PROCESSENTRY32);
  HANDLE snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
  if (::Process32First(snapshot, &entry)) {
    while (not exists && ::Process32Next(snapshot, &entry)) {
      HANDLE handle = ::OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, entry.th32ProcessID);
      if (handle != nullptr) {
        SString path;
        path.Resize(MAX_PATH + 1);
        DWORD size = MAX_PATH;
        if (::QueryFullProcessImageName(handle, 0, &path[0], &size)) {
          path.Fit();
          exists = path == absPath;
        }
        ::CloseHandle(handle);
      }
    }
  }
  ::CloseHandle(snapshot);
  return exists;
}

SString Sia::Api::PlatformSecureEncryptString(const SString &key, const SString &data) {
  auto convKey = SString::FromUtf8(key);
  auto convData = SString::FromUtf8(data);
  DATA_BLOB inputData = {0};
  inputData.pbData = (BYTE *)convData.c_str();
  inputData.cbData = convData.size() * sizeof(wchar_t);
  DATA_BLOB outputData = {0};
  if (::CryptProtectData(&inputData, &convKey[0], nullptr, nullptr, nullptr, 0, &outputData)) {
    std::vector<BYTE> encoded;
    Base64::Encode(outputData.pbData, outputData.cbData, encoded);
    return std::string(reinterpret_cast<const char *>(&encoded[0]), encoded.size());
  }
  return "";
}

bool Sia::Api::PlatformExecuteProcess(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, FilePath process, FilePath workingDir, const bool &waitForExit) {
  process.MakeAbsolute();
  workingDir.MakeAbsolute();
  STARTUPINFO si = {0};
  si.dwFlags = STARTF_USESHOWWINDOW;
#ifdef _DEBUG
  si.wShowWindow = SW_SHOW;
#else
  si.wShowWindow = SW_HIDE;
#endif
  si.cb = sizeof(si);
  SString command = process;
  if (static_cast<SString>(process).EndsWith(SIAD_EXECUTABLE)) {
    SString apiAdr = siaDriveConfig->GetHostNameOrIp() + ":" + SString::FromUInt32(siaDriveConfig->GetApiPort());
    SString hostAdr = ":" + SString::FromUInt32(siaDriveConfig->GetHostPort());
    SString rpcAddr = ":" + SString::FromUInt32(siaDriveConfig->GetRpcPort());
    command = "cmd.exe /c \"" + static_cast<SString>(process) + "\" --api-addr=" + apiAdr + " --host-addr=" + hostAdr + " --rpc-addr=" + rpcAddr;
  }
  PROCESS_INFORMATION pi;
  if (::CreateProcess(nullptr, &command[0], nullptr, nullptr, FALSE, 0, nullptr, &workingDir[0], &si, &pi)) {
    return true;
  }
  return false;
}

#ifdef SIADRIVE_PRIVATE_API
#include <siadokandrive.h>

std::unique_ptr<ISiaDrive> Sia::Api::PlatformCreateSiaDrive(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager) {
  return std::unique_ptr<ISiaDrive>(std::make_unique<Dokan::CDokanyDrive>(siaApi, transferManager));
}

#endif
