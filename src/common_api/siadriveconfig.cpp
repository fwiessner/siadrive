#include <siadriveconfig.h>
#include <fstream>
#include <utility>
#include <filepath.h>
#include <siacommon.h>
#include <platform.h>

using namespace Sia::Api;

CSiaDriveConfig::CSiaDriveConfig(const bool &allowSave, std::shared_ptr<ISiaApiCommunicator> siaComm) :
  CSiaDriveConfig(allowSave, SString(DEFAULT_CONFIG_FILE_PATH), std::move(siaComm)) {
}

CSiaDriveConfig::CSiaDriveConfig(const bool &allowSave, const SString &filePath, std::shared_ptr<ISiaApiCommunicator> siaComm) :
  _allowSave(allowSave),
  _siaComm(std::move(siaComm)),
  _FilePath(FilePath(filePath).Resolve()) {
  Load();
}

CSiaDriveConfig::CSiaDriveConfig(const json &configDoc, std::shared_ptr<ISiaApiCommunicator> siaComm) :
  _allowSave(false),
  _siaComm(std::move(siaComm)),
  _configDocument(configDoc) {
  siaComm->SetHostConfig(this->GetHostConfig());
}

CSiaDriveConfig::~CSiaDriveConfig() = default;

bool CSiaDriveConfig::LoadDefaults() {
  bool changed = false;
  if (not CheckDriveApiPrivatePort()) {
    SetDriveApiPrivatePort(11110);
    changed = true;
  }
  if (not CheckDriveApiPublicPort()) {
    SetDriveApiPublicPort(11111);
    changed = true;
  }
  if (not CheckRenter_TransferDbFilePath()) {
    SetRenter_TransferDbFilePath(static_cast<SString>(FilePath(DEFAULT_RENTER_TRANSFER_DB_FILE_PATH).Resolve()));
    changed = true;
  }
  if (not CheckVFS_SQLiteDbFilePath()) {
    SetVFS_SQLiteDbFilePath(static_cast<SString>(FilePath(DEFAULT_VFS_DB_FILE_PATH).Resolve()));
    changed = true;
  }
  if (not CheckCacheFolder()) {
    FilePath cacheFolder(FilePath::GetAppDataDirectory(), "siadrive");
    cacheFolder.Append("cache");
    SetCacheFolder(static_cast<SString>(cacheFolder));
    changed = true;
  }
  if (not CheckHostNameOrIp()) {
    SetHostNameOrIp("localhost");
    changed = true;
  }
  if (not CheckSiaApiPort()) {
#ifdef _WIN32
    SetSiaApiPort(GetRegistry("ApiPort", 9980));
#else
    SetSiaApiPort(9980);
#endif
    changed = true;
  }
  if (not CheckSiaHostPort()) {
#ifdef _WIN32
    SetSiaHostPort(GetRegistry("HostPort", 9982));
#else
    SetSiaHostPort(9982);
#endif
    changed = true;
  }
  if (not CheckSiaRpcPort()) {
#ifdef _WIN32
    SetSiaRpcPort(GetRegistry("RpcPort", 9981));
#else
    SetSiaRpcPort(9981);
#endif
    changed = true;
  }
  if (not CheckMaxUploadCount()) {
    SetMaxUploadCount(5);
    changed = true;
  }
  if (not CheckLockWalletOnExit()) {
    SetLockWalletOnExit(false);
    changed = true;
  }

  if (not CheckLaunchBundledSiad()) {
    SetLaunchBundledSiad(true);
    changed = true;
  }
  if (not CheckStopBundledSiad()) {
    SetStopBundledSiad(false);
    changed = true;
  }
  if (not CheckEventLevel()) {
#ifdef _DEBUG
    SetEventLevel("Debug");
#else
    SetEventLevel("Normal");
#endif
    changed = true;
  }
  if (not CheckStoreUnlockPassword()) {
    SetStoreUnlockPassword(false);
    changed = true;
  }
  if (not CheckLastMountLocation()) {
    SetLastMountLocation("");
    changed = true;
  }
  if (not CheckSiaCommTimeoutMs()) {
    SetSiaCommTimeoutMs(60000);
    changed = true;
  }
  if (not CheckLogDirectory()) {
    SetLogDirectory((SString)FilePath("~\\siadrive\\logs\\").Resolve());
    changed = true;
  }
  if (not CheckDownloadTimeoutSeconds()) {
    SetDownloadTimeoutSeconds(30);
    changed = true;
  }
  if (not CheckEnableDriveEvents()) {
#ifdef DEBUG
    SetEnableDriveEvents(true);
#else
    SetEnableDriveEvents(false);
#endif
    changed = true;
  }
  if (not CheckEnableMaximumCacheSize()) {
    SetEnableMaximumCacheSize(true);
    changed = true;
  }
  if (not CheckMaximumCacheSize()) {
    SetMaximumCacheSize(20ull * (1024 * 1024 * 1024));
    changed = true;
  }
  if (not CheckMinimumRedundancy()) {
    SetMinimumRedundancy(2.5);
    changed = true;
  }
  if (not CheckEvictionDelay()) {
    SetEvictionDelay(5);
    changed = true;
  }
  return changed;
}

void CSiaDriveConfig::Load() {
  FilePath filePath = GetFilePath();
  std::ifstream myfile(&filePath[0]);
  if (myfile.is_open()) {
    std::stringstream ss;
    ss << myfile.rdbuf();
    std::string jsonTxt = ss.str();
    if (jsonTxt.empty()) {
      myfile.close();
      if (LoadDefaults()) {
        Save();
      }
    } else {
      _configDocument = json::parse(jsonTxt.c_str());
      myfile.close();
    }
  }

  if (LoadDefaults()) {
    Save();
  }
  _siaComm->SetHostConfig(this->GetHostConfig());

  FilePath cacheFolder(GetCacheFolder());
  if (not cacheFolder.IsDirectory()) {
    cacheFolder.MakeDirectory();
  }
}

bool CSiaDriveConfig::Save() {
  if (_allowSave) {
    FilePath filePath = GetFilePath();
    FilePath folder = filePath;
    folder.RemoveFileName();
    if (not folder.IsDirectory()) {
      folder.MakeDirectory();
    }

    std::lock_guard<std::mutex> l(_saveLock);
    return RetryAction([&]()->BOOL {
      try {
        std::ofstream(static_cast<SString>(filePath).str().c_str()) << std::setw(2) << _configDocument << std::endl;
        return TRUE;
      } catch (...) {
        filePath.DeleteAsFile();
      }
      return FALSE;
    }, DEFAULT_RETRY_COUNT, DEFAULT_RETRY_DELAY_MS);
  }

  return false;
}

SString CSiaDriveConfig::ToString() const {
  return _configDocument.dump();
}

SiaHostConfig CSiaDriveConfig::GetHostConfig() const {
  SiaHostConfig hostConfig;
  hostConfig.HostName = GetHostNameOrIp();
  hostConfig.HostPort = GetSiaApiPort();
  hostConfig.RequiredVersion = COMPAT_SIAD_VERSION;
  hostConfig.TimeoutMs = GetSiaCommTimeoutMs();
  return hostConfig;
}

std::shared_ptr<ISiaApiCommunicator> CSiaDriveConfig::GetSiaCommunicator() const {
  return _siaComm;
}
