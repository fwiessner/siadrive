#include <siaapi.h>
#include <siadriveconfig.h>

#include <utility>

using namespace Sia::Api;

CSiaApi::_CSiaHostDb::_CSiaHostDb(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  CSiaBase(std::move(siaDriveConfig)) {
}

CSiaApi::_CSiaHostDb::~_CSiaHostDb() = default;

void CSiaApi::_CSiaHostDb::Refresh() {
}

CSiaHostCollectionPtr CSiaApi::_CSiaHostDb::GetActiveHosts() const {
  CSiaHostCollectionPtr hostCollection(new CSiaHostCollection());
  json result;
  if (ApiSuccess(GetSiaDriveConfig()->GetSiaCommunicator()->Get("/hostdb/active", result))) {
    auto hosts = result["hosts"];
    for (const auto &host : hosts) {
      CSiaHostPtr hostPtr(new CSiaHost(GetSiaDriveConfig(), host));
      hostCollection->push_back(hostPtr);
    }
  }
  return hostCollection;
}
