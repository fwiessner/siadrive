#include <vfs/sqliteitemstore.h>

using namespace Sia::Api;

BEGIN_EVENT(UpgradeProgress)
  public:
    UpgradeProgress(const std::uint64_t &totalItems, const std::uint64_t &remainItems) :
      CEvent(EventLevel::Normal),
      _totalItems(totalItems),
      _remainItems(remainItems) {
    }

  private:
    UpgradeProgress()  :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~UpgradeProgress() {
    }

  private:
    std::uint64_t _totalItems;
    std::uint64_t _remainItems;

  protected:
    virtual void LoadData(const json& j) override {
      _totalItems = j["total_items"].get<std::uint64_t>();
      _remainItems = j["remain_items"].get<std::uint64_t>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new UpgradeProgress(_totalItems, _remainItems));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|TOTAL|" + SString::FromUInt64( _totalItems) + "|REMAIN|" + SString::FromUInt64(_remainItems);
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"total_items",  _totalItems},
              {"remain_items", _remainItems}};
    }
END_EVENT(UpgradeProgress);

BEGIN_EVENT(CurrentUpgradeProgress)
  public:
    CurrentUpgradeProgress(const std::uint64_t &totalItems, const std::uint64_t &remainItems) :
      CEvent(EventLevel::Normal),
      _totalItems(totalItems),
      _remainItems(remainItems) {
    }

  private:
    CurrentUpgradeProgress()  :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~CurrentUpgradeProgress() {
    }

  private:
    std::uint64_t _totalItems;
    std::uint64_t _remainItems;

  protected:
    virtual void LoadData(const json& j) override {
      _totalItems = j["total_items"].get<std::uint64_t>();
      _remainItems = j["remain_items"].get<std::uint64_t>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new CurrentUpgradeProgress(_totalItems, _remainItems));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|TOTAL|" + SString::FromUInt64( _totalItems) + "|REMAIN|" + SString::FromUInt64(_remainItems);
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"total_items",  _totalItems},
              {"remain_items", _remainItems}};
    }
END_EVENT(CurrentUpgradeProgress);


BEGIN_EVENT(VFSError)
  public:
    VFSError(const SString &func, const SString& reason) :
      CEvent(EventLevel::Error),
      _func(func),
      _reason(reason) {
    }

  private:
    VFSError()  :
      CEvent(EventLevel::Error) {
    }

  public:
    virtual ~VFSError() {
    }

  private:
    SString _func;
    SString _reason;

  protected:
    virtual void LoadData(const json& j) override {
      _func = j["function_name"].get<std::string>();
      _reason = j["reason"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new VFSError(_func, _reason));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|FUNC|" + _func + "|REASON|" + _reason;
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"function_name",  _func},
              {"reason", _reason}};
    }
END_EVENT(VFSError);

CEventPtr Sia::Api::CreateVFSErrorEvent(const SString &func, const SString &reason) {
  return std::dynamic_pointer_cast<CEvent>(std::make_shared<VFSError>(func, reason));
}

CEventPtr Sia::Api::CreateUpgradeProgressEvent(const std::uint64_t& total, const std::uint64_t& remain) {
  return std::dynamic_pointer_cast<CEvent>(std::make_shared<UpgradeProgress>(total, remain));
}

CEventPtr Sia::Api::CreateCurrentUpgradeProgressEvent(const std::uint64_t& total, const std::uint64_t& remain) {
  return std::dynamic_pointer_cast<CEvent>(std::make_shared<CurrentUpgradeProgress>(total, remain));
}
