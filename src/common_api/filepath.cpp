#include <filepath.h>
#include <regex>
#if defined(__linux__) || defined(__APPLE__)

#include <unistd.h>
#include <pwd.h>
#include <sys/stat.h>
#include <libgen.h>
#include <climits>
#include <dirent.h>
#include <fcntl.h>
#include <sys/statvfs.h>
#endif
#ifdef _WIN32
#include <Shlobj.h>

#endif
using namespace Sia::Api;

std::uint64_t FilePath::GetRemainingSpace(const FilePath& folder) {
#ifdef _WIN32
  ULARGE_INTEGER li={0};
  ::GetDiskFreeSpaceEx(&folder[0], &li, nullptr, nullptr);
  return li.QuadPart;
#else
  struct statvfs buf{};
  auto ret = statvfs(&folder[0], &buf);
  return (ret == -1) ? 0 : (buf.f_bfree * buf.f_frsize);
#endif
}

std::uint64_t FilePath::CalculateUsedSpace(const FilePath &folder, const bool &recursive) {
  std::uint64_t ret = 0;
#ifdef _WIN32
  WIN32_FIND_DATA fd = {0};
  SString search;
  search.Resize(MAX_PATH + 1);
  ::PathCombine(&search[0], &folder[0], "*.*");

  HANDLE find = ::FindFirstFile(search.str().c_str(), &fd);
  if (find != INVALID_HANDLE_VALUE) {
    do {
      if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        if (recursive && (SString(fd.cFileName) != ".") && (SString(fd.cFileName) != "..")) {
          SString nextFolder;
          nextFolder.Resize(MAX_PATH + 1);
          ::PathCombine(&nextFolder[0], &folder[0], fd.cFileName);
          ret += CalculateUsedSpace(nextFolder, recursive);
        }
      } else {
        SString filePath;
        filePath.Resize(MAX_PATH + 1);
        ::PathCombine(&filePath[0], &folder[0], fd.cFileName);
        ret += FilePath::FileSize(filePath);
      }
    } while (::FindNextFile(find, &fd) != 0);
  }
#else
  DIR *root = opendir(&folder[0]);
  if (root) {
    struct dirent *de;
    while ((de = readdir(root)) != nullptr) {
      if (de->d_type == DT_DIR) {
        if (recursive && (strcmp(de->d_name, ".") != 0) && (strcmp(de->d_name, "..") != 0)) {
          ret += CalculateUsedSpace(folder + de->d_name, recursive);
        }
      } else {
        FilePath f = folder + de->d_name;
        ret += FilePath::FileSize(f);
      }
    }
    closedir(root);
  }
#endif

  return ret;
}


std::vector<FilePath> FilePath::FindAllDirectories(const FilePath &folder, const bool &recursive) {
  std::vector<FilePath> ret;
#ifdef _WIN32
  WIN32_FIND_DATA fd = { 0 };
  SString search;
  search.Resize(MAX_PATH + 1);
  ::PathCombine(&search[0], &folder[0], "*.*");
  HANDLE find = ::FindFirstFile(search.str().c_str(), &fd);
  if (find != INVALID_HANDLE_VALUE) {
    do {
      if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        if ((SString(fd.cFileName) != ".") && (SString(fd.cFileName) != "..")) {
          SString nextFolder;
          nextFolder.Resize(MAX_PATH + 1);
          ::PathCombine(&nextFolder[0], &folder[0], fd.cFileName);
          if (recursive) {
            auto tmp = FindAllDirectories(nextFolder, recursive);
            ret.insert(ret.end(), tmp.begin(), tmp.end());
          }
          ret.push_back(nextFolder);
        }
      }
    } while (::FindNextFile(find, &fd) != 0);
  }
#else
  DIR *root = opendir(&folder[0]);
  if (root) {
    struct dirent *de;
    while ((de = readdir(root)) != nullptr) {
      if (de->d_type == DT_DIR) {
        if ((strcmp(de->d_name, ".") != 0) && (strcmp(de->d_name, "..") != 0)) {
          if (recursive) {
            auto tmp = FindAllDirectories(folder + de->d_name, recursive);
            ret.insert(ret.end(), tmp.begin(), tmp.end());
          }

          ret.push_back(folder + de->d_name);
        }
      }
    }
    closedir(root);
  }
#endif

  return std::move(ret);
}

std::vector<FilePath> FilePath::FindAllFiles(const FilePath &folder, const bool &recursive, const std::int64_t &minSize) {
  std::vector<FilePath> ret;
#ifdef _WIN32
  WIN32_FIND_DATA fd = {0};
  SString search;
  search.Resize(MAX_PATH + 1);
  ::PathCombine(&search[0], &folder[0], "*.*");
  HANDLE find = ::FindFirstFile(search.str().c_str(), &fd);
  if (find != INVALID_HANDLE_VALUE) {
    do {
      if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        if (recursive && (SString(fd.cFileName) != ".") && (SString(fd.cFileName) != "..")) {
          SString nextFolder;
          nextFolder.Resize(MAX_PATH + 1);
          ::PathCombine(&nextFolder[0], &folder[0], fd.cFileName);
          auto tmp = FindAllFiles(nextFolder, recursive, minSize);
          ret.insert(ret.end(), tmp.begin(), tmp.end());
        }
      } else {
        SString filePath;
        filePath.Resize(MAX_PATH + 1);
        ::PathCombine(&filePath[0], &folder[0], fd.cFileName);
        if (FilePath::FileSize(filePath) >= minSize) {
          ret.push_back(filePath);
        }
      }
    } while (::FindNextFile(find, &fd) != 0);
  }
#else
  DIR *root = opendir(&folder[0]);
  if (root) {
    struct dirent *de;
    while ((de = readdir(root)) != nullptr) {
      if (de->d_type == DT_DIR) {
        if (recursive && (strcmp(de->d_name, ".") != 0) && (strcmp(de->d_name, "..") != 0)) {
          auto tmp = FindAllFiles(folder + de->d_name, recursive, minSize);
          ret.insert(ret.end(), tmp.begin(), tmp.end());
        }
      } else {
        FilePath f = folder + de->d_name;
        if (FilePath::FileSize(f) >= minSize) {
          ret.emplace_back(f);
        }
      }
    }
    closedir(root);
  }
#endif

  return std::move(ret);
}

BOOL FilePath::RecursiveDeleteFilesByExtension(const FilePath &folder, const SString &extensionWithDot) {
#ifdef _WIN32
  BOOL ret = FALSE;
  WIN32_FIND_DATA fd = {0};
  SString search;
  search.Resize(MAX_PATH + 1);
  ::PathCombine(&search[0], &folder[0], "*.*");
  HANDLE find = ::FindFirstFile(search.str().c_str(), &fd);
  if (find != INVALID_HANDLE_VALUE) {
    ret = TRUE;
    do {
      if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        if ((SString(fd.cFileName) != ".") && (SString(fd.cFileName) != "..")) {
          SString nextFolder;
          nextFolder.Resize(MAX_PATH + 1);
          ::PathCombine(&nextFolder[0], &folder[0], fd.cFileName);
          ret = FilePath::RecursiveDeleteFilesByExtension(nextFolder, extensionWithDot);
        }
      } else {
        SString ext = ::PathFindExtension(fd.cFileName);
        if (ext == extensionWithDot) {
          SString filePath;
          filePath.Resize(MAX_PATH + 1);
          ::PathCombine(&filePath[0], &folder[0], fd.cFileName);
          ret = RetryDeleteFileIfExists(filePath);
        }
      }
    } while (ret && (::FindNextFile(find, &fd) != 0));
  }
  return ret;
#else
  BOOL ret = true;
  FilePath fp(folder);
  DIR *root = opendir(&fp[0]);
  if (root) {
    struct dirent *de;
    while (ret && (de = readdir(root))) {
      if (de->d_type == DT_DIR) {
        if ((strcmp(de->d_name, ".") != 0) && (strcmp(de->d_name, "..") != 0)) {
          ret = RecursiveDeleteFilesByExtension(fp + de->d_name, extensionWithDot);
        }
      } else if (SString(de->d_name).EndsWith(extensionWithDot)) {
        FilePath f = fp + de->d_name;
        ret = f.DeleteAsFile();
      }
    }
    closedir(root);
  }
  return ret;
#endif
}

BOOL FilePath::RetryDeleteFileIfExists(const FilePath &filePath) {
  BOOL ret = TRUE;
  if (filePath.IsFile()) {
    ret = RetryableAction(filePath.DeleteAsFile(), DEFAULT_RETRY_COUNT, DEFAULT_RETRY_DELAY_MS);
  }
  return ret;
}

std::int64_t FilePath::FileSize(const FilePath &filePath) {
#ifdef _WIN32
  struct _stat64 buf;
  if (_stat64(&filePath[0], &buf) != 0) {
#else
    struct stat64 buf;
    if (stat64(&filePath[0], &buf) != 0) {
#endif
    return -1;
  } // error, could use errno to find out more

  return buf.st_size;
}

SString FilePath::FinalizePath(const SString &path) {
  std::regex r(("\\" + NDirSep + "+").str());
  const SString str = std::regex_replace(path.str(), r, DirSep.str());
  std::regex r2((DirSep + DirSep + "+").str());
  SString ret = std::regex_replace(str.str(), r2, DirSep.str());
  if ((ret.Length() > 1) && (ret[ret.Length() - 1] == DirSep[0])) {
    ret = ret.SubString(0, ret.Length() - 1);
  }
  return std::move(ret);
}

SString FilePath::GetTempDirectory() {
  // TODO path temp_directory_path();  path temp_directory_path(std::error_code& ec); from c++17

#ifdef _WIN32
  SString tempPath;
  tempPath.Resize(MAX_PATH + 1);
  GetTempPath(MAX_PATH, &tempPath[0]);
  return std::move(tempPath.Fit());
#else
  return P_tmpdir;
#endif
}

SString FilePath::GetAppDataDirectory() {
#ifdef _WIN32
  ComInitWrapper cw;
  PWSTR localAppData = nullptr;
  ::SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, nullptr, &localAppData);
  SString ret = localAppData;
  ::CoTaskMemFree(localAppData);
  return ret;
#endif
#ifdef __linux__
  struct passwd *pw = getpwuid(getuid());
  FilePath ret = SString(pw->pw_dir);
  ret.Append(".config");
  return ret;
#endif
#ifdef __APPLE__
  struct passwd *pw = getpwuid(getuid());
  FilePath ret = SString(pw->pw_dir);
  ret.Append("Library").Append("Application Support");
  return ret;
#endif
}

FilePath::FilePath() {
}

FilePath::FilePath(const FilePath &filePath) :
  _path(filePath._path) {
}

FilePath::FilePath(const SString &path) {
  _path = FinalizePath(path);
}

FilePath::FilePath(const FilePath &filePath1, const FilePath &filePath2) {
  _path = filePath1;
  Append(filePath2);
}

FilePath::FilePath(const FilePath &filePath1, const SString &path2) {
  _path = filePath1;
  Append(FinalizePath(path2));
}

FilePath::FilePath(const SString &path1, const FilePath &filePath2) {
  _path = FinalizePath(path1);
  Append(filePath2);
}

FilePath::FilePath(const SString &path1, const SString &path2) {
  _path = FinalizePath(path1);
  Append(FinalizePath(path2));
}

FilePath::FilePath(FilePath &&filePath) :
  _path(std::move(filePath._path)) {
}

FilePath::~FilePath() {
}

#ifdef _WIN32
const SString FilePath::DirSep = "\\";
const SString FilePath::NDirSep = "/";
#else
const SString FilePath::DirSep = "/";
const SString FilePath::NDirSep = "\\";
#endif

FilePath &FilePath::Append(const FilePath &filePath) {
  _path = FinalizePath(_path + DirSep + filePath.GetPath());
  return *this;
}

FilePath &FilePath::Append(const SString &path) {
  _path = FinalizePath(_path + DirSep + path);
  return *this;
}

bool FilePath::IsDirectory() const {
#ifdef _WIN32
  return ::PathIsDirectory(&_path[0]) ? true : false;
#else
  struct stat sb;
  return (not stat(&_path[0], &sb) && S_ISDIR(sb.st_mode));
#endif
}

bool FilePath::IsFile() const {
#ifdef _WIN32
  return (::PathFileExists(&_path[0]) && not ::PathIsDirectory(&_path[0]));
#else
  struct stat sb;
  return (not stat(&_path[0], &sb) && not S_ISDIR(sb.st_mode)); // TODO Or S_ISREG????
#endif
}

bool FilePath::IsUNC() const {
#ifdef _WIN32
  return ::PathIsUNC(&_path[0]) ? true : false;
#else
  return false; // Not really supported on non-Windows
#endif
}

bool FilePath::MakeDirectory() const {
  SString path = FilePath(_path).MakeAbsolute();
#ifdef _WIN32
  auto convString = SString::FromUtf8(path);
  return (::SHCreateDirectory(nullptr, &convString[0]) == ERROR_SUCCESS);
#else
  bool ret = true;
  const auto paths = path.Split(FilePath::DirSep[0]);
  for (size_t i = 0; ret && (i < paths.size()); i++) {
    if (paths[i].Length()) { // Skip root
      const auto status = mkdir(&path[0], S_IRWXU);
      ret = ((status == 0) || (errno == EEXIST));
    }
  }
  return ret;
#endif
}

bool FilePath::DeleteAsDirectory() const {
#ifdef _WIN32
  return not IsDirectory() || ::RemoveDirectory(&_path[0]) ? true : false;
#else
  return not IsDirectory() || (rmdir(&_path[0]) == 0);
#endif
}

bool FilePath::CopyAsFile(const FilePath &dest) const {
#ifdef _WIN32
  return ::CopyFile(&_path[0], &dest[0], FALSE) ? true : false;
#else
  int in_fd = open(&_path[0], O_RDONLY, 0600u);
  int out_fd = open(&dest[0], O_WRONLY, 0600u);
  const int BUF_SIZE = 8192;
  char buf[BUF_SIZE];

  bool ret = ((in_fd >= 0) && (out_fd >= 0));
  while (ret) {
    auto result = read(in_fd, &buf[0], BUF_SIZE);
    if (result <= 0) {
      fsync(out_fd);
      break;
    }

    ret = (write(out_fd, &buf[0], result) != -1);
  }

  close(in_fd);
  close(out_fd);
  return ret;
#endif
}

bool FilePath::DeleteAsFile() const {
#ifdef _WIN32
  return not IsFile() || ::DeleteFile(&_path[0]) ? true : false;
#else
  return not IsFile() || (unlink(&_path[0]) == 0);
#endif
}

bool FilePath::MoveAsFile(const FilePath &filePath) {
  FilePath folder(filePath);
  folder.RemoveFileName().MakeDirectory();
#ifdef _WIN32
  return ::MoveFile(&_path[0], &filePath[0]) ? true : false;
#else
  return (rename(&_path[0], &filePath[0]) == 0);
#endif
}

const SString &FilePath::GetPath() const {
  return _path;
}

FilePath &FilePath::RemoveFileName() {
#ifdef _WIN32
  ::PathRemoveFileSpec(&_path[0]);
  _path = _path.str().c_str();
#else
  SString tmp(_path);
  _path = dirname(&tmp[0]);
#endif
  return *this;
}

FilePath FilePath::operator+(const FilePath &filePath) const {
  return FilePath(*this, filePath);
}

FilePath FilePath::operator+(const SString &filePath) const {
  return FilePath(*this, filePath);
}

FilePath &FilePath::operator+=(const FilePath &filePath) {
  return Append(filePath);
}

bool FilePath::operator==(const FilePath &filePath) const {
  if (this != &filePath) {
    return filePath._path == _path;
  }
  return true;
}

bool FilePath::operator==(FilePath &&filePath) const {
  if (this != &filePath) {
    return filePath._path == _path;
  }
  return true;
}

bool FilePath::operator!=(const FilePath &filePath) const {
  if (this != &filePath) {
    return filePath._path != _path;
  }
  return false;
}

bool FilePath::operator!=(FilePath &&filePath) const {
  if (this != &filePath) {
    return filePath._path != _path;
  }
  return false;
}

FilePath &FilePath::operator=(const FilePath &filePath) {
  if (this != &filePath) {
    _path = filePath._path;
  }
  return *this;
}

FilePath &FilePath::operator=(FilePath &&filePath) {
  if (this != &filePath) {
    _path = std::move(filePath._path);
  }
  return *this;
}

SString::SChar &FilePath::operator[](const size_t &idx) {
  return _path[idx];
}

const SString::SChar &FilePath::operator[](const size_t &idx) const {
  return _path[idx];
}

FilePath::operator SString() {
  return _path;
}

FilePath::operator SString() const {
  return _path;
}

FilePath &FilePath::MakeAbsolute() {
#ifdef _WIN32
  if (::PathIsRelative(&_path[0])) {
    SString temp;
    temp.Resize(MAX_PATH + 1);
    _path = _fullpath(&temp[0], &_path[0], MAX_PATH);
  }
#else
  char full[PATH_MAX + 1] = {0};
  realpath(&_path[0], full);
  _path = full;
#endif
  return *this;
}

FilePath &FilePath::StripToFileName() {
#ifdef _WIN32
  _path = ::PathFindFileName(&_path[0]);
#else
  SString tmp(_path);
  _path = basename(&tmp[0]);
#endif
  return *this;
}

bool FilePath::CreateEmptyFile() const {
  FILE *fp = fopen(_path.str().c_str(), "w+");
  if (fp) {
    fclose(fp);
    return true;
  }
  return false;
}

std::uint64_t FilePath::GetModifiedTime() const {
  struct stat st{};
#ifdef _WIN32
  std::string tmp = &_path[0];
  int res = stat(&tmp[0], &st);
  return (res == 0) ? st.st_mtime : (std::uint64_t) -1;
#else
  int res = stat(&_path[0], &st);
#ifdef __linux__
  return (res == 0) ? st.st_mtim.tv_sec : (std::uint64_t)-1;
#endif
#ifdef __APPLE__
  return (res == 0) ? *reinterpret_cast<std::uint64_t *>(&st.st_mtimespec.tv_sec) : (std::uint64_t)-1;
#endif
#endif
}

FilePath &FilePath::Resolve() {
  _path = FinalizePath(_path.Replace("~", GetAppDataDirectory()));
  return *this;
}

bool FilePath::IsParentOf(const FilePath &filePath) const {
  return filePath._path.BeginsWith(_path + DirSep);
}

SString FilePath::GetExtension(const bool& includeDot) const {
  auto ext = ((SString)FilePath(_path).StripToFileName()).Split('.');
  if (ext.size() > 1) {
    return includeDot ? "." : "" + ext[ext.size() - 1];
  }

  return "";
}