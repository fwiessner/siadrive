#include <eventsystem.h>
#include <chrono>
#include <filepath.h>
#include <siaapi.h>

using namespace Sia::Api;
using namespace std::chrono_literals;
CEventSystem CEventSystem::EventSystem;

EventLevel Sia::Api::EventLevelFromString(const SString &eventLevel) {
  if (eventLevel == "Debug" || eventLevel == "EventLevel::Debug") {
    return EventLevel::Debug;
  } else if (eventLevel == "Warn" || eventLevel == "EventLevel::Warn") {
    return EventLevel::Warn;
  } else if (eventLevel == "Normal" || eventLevel == "EventLevel::Normal") {
    return EventLevel::Normal;
  } else if (eventLevel == "Error" || eventLevel == "EventLevel::Error") {
    return EventLevel::Error;
  } else if (eventLevel == "Verbose" || eventLevel == "EventLevel::Verbose") {
    return EventLevel::Verbose;
  }
  return EventLevel::Normal;
}

CEventSystem::CEventSystem() = default;

CEventSystem::~CEventSystem() {
  Stop();
}

void CEventSystem::RegisterJsonFactory(const SString& eventName, std::function<CEventPtr(const json& j)> factory) {
  _registerMap.insert({eventName, factory});
}

void CEventSystem::ProcessEvents() {
  do {
    CEventPtr eventData;
    do {
      {
        std::lock_guard<std::mutex> l(_eventMutex);
        if (_eventQueue.size()) {
          eventData = _eventQueue.front();
          _eventQueue.pop_front();
        } else {
          eventData.reset();
        }
      }
      if (eventData) {
        for (auto &v : _eventConsumers) {
          v(*eventData);
        }
        {
          std::lock_guard<std::mutex> l(_oneShotMutex);
          std::deque<std::function<bool(const CEvent &)>> consumerList;
          for (auto &v : _oneShotEventConsumers) {
            if (not v(*eventData)) {
              consumerList.push_back(v);
            }
          }
          _oneShotEventConsumers = consumerList;
        }
      }
    } while (eventData);
    std::unique_lock<std::mutex> l(_stopMutex);
    _stopEvent.wait_for(l, 10ms);
  } while (not _stopRequested);
}

void CEventSystem::NotifyEvent(CEventPtr eventData) {
  if (eventData) {
    std::lock_guard<std::mutex> l(_eventMutex);
    if (_eventConsumers.size() || _oneShotEventConsumers.size()) {
      _eventQueue.push_back(eventData);
    }

    for (auto &fwd : _forwarderMap) {
      fwd->ForwardEvent(eventData->GetEventJson());
    }
  }
}

void CEventSystem::NotifyJsonEvent(const json& j) {
  auto eventData = CreateJsonEvent(j);
  if (eventData) {
    std::lock_guard<std::mutex> l(_eventMutex);
    if (_eventConsumers.size() || _oneShotEventConsumers.size()) {
      _eventQueue.push_back(eventData);
    }
  }
}

void CEventSystem::AddEventConsumer(std::function<void(const CEvent &)> consumer) {
  std::lock_guard<std::mutex> l(_eventMutex);
  _eventConsumers.push_back(consumer);
}

void CEventSystem::AddEventForwarder(IEventForwarder* eventForwarder) {
  if (not _processThread && eventForwarder) {
    _forwarderMap.push_back(eventForwarder);
  }
}

void CEventSystem::AddOneShotEventConsumer(std::function<bool(const CEvent &)> consumer) {
  std::lock_guard<std::mutex> l(_oneShotMutex);
  _oneShotEventConsumers.push_back(consumer);
}

CEventPtr CEventSystem::CreateJsonEvent(const json& j) {
  if (j.find("event") != j.end()) {
    SString name = j["event"].get<std::string>();
    if (_registerMap.find(name) != _registerMap.end()) {
      return _registerMap[name](j);
    }
  }

  return nullptr;
}

void CEventSystem::Start() {
  if (not _processThread) {
    _stopRequested = false;
    _processThread.reset(new std::thread([this]() {
      ProcessEvents();
    }));
  }
}

void CEventSystem::Stop() {
  if (_processThread) {
    _stopRequested = true;
    _stopEvent.notify_all();
    _processThread->join();
    _processThread.reset();

    // Drain events
    while (_eventQueue.size()) {
      CEventPtr eventData;
      {
        std::lock_guard<std::mutex> l(_eventMutex);
        eventData = _eventQueue.front();
        _eventQueue.pop_front();
      }
      for (auto &v : _eventConsumers) {
        v(*eventData);
      }
      {
        std::lock_guard<std::mutex> l(_oneShotMutex);
        for (auto &v : _oneShotEventConsumers) {
          v(*eventData);
        }
      }
    }
    {
      std::lock_guard<std::mutex> l(_eventMutex);
      _eventConsumers.clear();
    }
    {
      std::lock_guard<std::mutex> l(_oneShotMutex);
      _oneShotEventConsumers.clear();
    }
    _stopRequested = false;
  }
}

NS_BEGIN(Sia)
NS_BEGIN(Api)
BEGIN_EVENT(SystemCriticalEvent)
  public:
    SystemCriticalEvent(const SString &reason) :
      CEvent(EventLevel::Error),
      _reason(reason) {
    }

    virtual ~SystemCriticalEvent() {
    }

  private:
    SystemCriticalEvent()  :
      CEvent(EventLevel::Error) {
    }

  private:
    SString _reason;

  protected:
    virtual void LoadData(const json& j) override {
      _reason = j["reason"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|REASON|" + _reason;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::static_pointer_cast<CEvent>(std::make_shared<SystemCriticalEvent>(_reason));
    }

    virtual json GetEventJson() const override {
      return {{"event",  GetEventName()},
              {"reason", _reason}};
    }
END_EVENT(SystemCriticalEvent);

BEGIN_EVENT(SiaDriveFileClosed)
  public:
    SiaDriveFileClosed(const SString &siaPath, const FilePath &filePath, const bool &wasChanged) :
      CEvent(EventLevel::Debug),
      _siaPath(siaPath),
      _filePath(filePath),
      _wasChanged(wasChanged) {
    }

  private:
    SiaDriveFileClosed()  :
      CEvent(EventLevel::Debug) {
    }

  public:
    virtual ~SiaDriveFileClosed() {
    }

  private:
    SString _siaPath;
    FilePath _filePath;
    bool _wasChanged;

  protected:
    virtual void LoadData(const json& j) override {
      _filePath = FilePath(j["file_path"].get<std::string>());
      _siaPath = j["sia_path"].get<std::string>();
      _wasChanged = j["changed"].get<bool>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveFileClosed>(_siaPath, _filePath, _wasChanged));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath + "|CHANGED|" + SString::FromBool(_wasChanged);
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"file_path", static_cast<SString>(_filePath)},
              {"sia_path",  _siaPath},
              {"changed",   _wasChanged}};
    }
END_EVENT(SiaDriveFileClosed);

BEGIN_EVENT(SiaDriveCacheFileAdded)
  public:
    SiaDriveCacheFileAdded(const SString &siaPath, const FilePath &filePath, const bool &succeeded, const SString &reason) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath),
      _succeeded(succeeded),
      _reason(reason) {
    }

  public:
    virtual ~SiaDriveCacheFileAdded() {
    }

  private:
    SiaDriveCacheFileAdded() :
      CEvent(EventLevel::Normal) {
    }

  private:
    SString _siaPath;
    FilePath _filePath;
    bool _succeeded;
    SString _reason;

  protected:
    virtual void LoadData(const json& j) override {
      _filePath = FilePath(j["file_path"].get<std::string>());
      _siaPath = j["sia_path"].get<std::string>();
      _succeeded = j["succeeded"].get<bool>();
      _reason = j["reason"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath + "|OK|" + SString::FromBool(_succeeded) + (_reason.Length() ? "|MSG|" + _reason : "");
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaDriveCacheFileAdded(_siaPath, _filePath, _succeeded, _reason));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"file_path", static_cast<SString>(_filePath)},
              {"reason",    _reason},
              {"sia_path",  _siaPath},
              {"succeeded", _succeeded}};
    }
END_EVENT(SiaDriveCacheFileAdded);

BEGIN_EVENT(SiaDriveDownloadToCacheBegin)
  public:
    SiaDriveDownloadToCacheBegin(const SString &siaPath, const FilePath &filePath, const FilePath &tempFilePath) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath),
      _tempFilePath(tempFilePath) {
    }

  public:
    virtual ~SiaDriveDownloadToCacheBegin() {
    }

  private:
    SiaDriveDownloadToCacheBegin()  :
      CEvent(EventLevel::Normal) {
    }

  private:
    SString _siaPath;
    FilePath _filePath;
    FilePath _tempFilePath;

  protected:
    virtual void LoadData(const json& j) override {
      _filePath = FilePath(j["file_path"].get<std::string>());
      _siaPath = j["sia_path"].get<std::string>();
      _tempFilePath = FilePath(j["temp_file_path"].get<std::string>());
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath + "|TP|" + _tempFilePath;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveDownloadToCacheBegin>(_siaPath, _filePath, _tempFilePath));
    }

    virtual json GetEventJson() const override {
      return {{"event",          GetEventName()},
              {"file_path",      static_cast<SString>(_filePath)},
              {"sia_path",       _siaPath},
              {"temp_file_path", static_cast<SString>(_tempFilePath)}};
    }
END_EVENT(SiaDriveDownloadToCacheBegin);

BEGIN_EVENT(SiaDriveDownloadToCacheEnd)
  public:
    SiaDriveDownloadToCacheEnd(const SString &siaPath, const FilePath &filePath, const FilePath &tempFilePath, const SiaApiError &result) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath),
      _tempFilePath(tempFilePath),
      _result(result.GetReason()) {
    }

  private:
    SiaDriveDownloadToCacheEnd(const SString &siaPath, const FilePath &filePath, const FilePath &tempFilePath, const SString &result) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath),
      _tempFilePath(tempFilePath),
      _result(result) {
    }

  private:
    SiaDriveDownloadToCacheEnd()  :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~SiaDriveDownloadToCacheEnd() {
    }

  private:
    SString _siaPath;
    FilePath _filePath;
    FilePath _tempFilePath;
    SString _result;

  protected:
    virtual void LoadData(const json& j) override {
      _filePath = FilePath(j["file_path"].get<std::string>());
      _siaPath = j["sia_path"].get<std::string>();
      _tempFilePath = FilePath(j["temp_file_path"].get<std::string>());
      _result = j["result"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath + "|TP|" + _tempFilePath + "|RES|" + _result;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaDriveDownloadToCacheEnd(_siaPath, _filePath, _tempFilePath, _result));
    }

    virtual json GetEventJson() const override {
      return {{"event",          GetEventName()},
              {"file_path",      static_cast<SString>(_filePath)},
              {"sia_path",       _siaPath},
              {"temp_file_path", static_cast<SString>(_tempFilePath)},
              {"result",         _result}};
    }
END_EVENT(SiaDriveDownloadToCacheEnd);

BEGIN_EVENT(SiaDriveFileUnrecoverable)
  public:
    SiaDriveFileUnrecoverable(const SString &siaPath, const FilePath &filePath) :
      CEvent(EventLevel::Error),
      _siaPath(siaPath),
      _filePath(filePath) {
    }

  private:
    SiaDriveFileUnrecoverable() :
      CEvent(EventLevel::Error) {
    }

  public:
    virtual ~SiaDriveFileUnrecoverable() {
    }

  private:
    SString _siaPath;
    FilePath _filePath;

  protected:
    virtual void LoadData(const json& j) override {
      _filePath = FilePath(j["file_path"].get<std::string>());
      _siaPath = j["sia_path"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveFileUnrecoverable>(_siaPath, _filePath));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"file_path", static_cast<SString>(_filePath)},
              {"sia_path",  _siaPath}};
    }
END_EVENT(SiaDriveFileUnrecoverable);

BEGIN_EVENT(SiaDriveMountEnded)
  public:
    SiaDriveMountEnded(const SString mountLocation, const std::int32_t &result) :
      CEvent(EventLevel::Normal),
      _mountLocation(mountLocation),
      _result(result) {
    }

  private:
    SiaDriveMountEnded() :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~SiaDriveMountEnded() {
    }

  private:
    SString _mountLocation;
    std::int32_t _result;

  protected:
    virtual void LoadData(const json& j) override {
      _mountLocation = j["mount_location"].get<std::string>();
      _result = j["result"].get<std::int32_t>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaDriveMountEnded(_mountLocation, _result));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|LOC|" + _mountLocation + "|RES|" + SString::FromInt32(_result);
    }

    virtual json GetEventJson() const override {
      return {{"event",          GetEventName()},
              {"mount_location", _mountLocation},
              {"result",         _result}};
    }
END_EVENT(SiaDriveMountEnded);

BEGIN_EVENT(SiaDriveUnMounted)
  public:
    SiaDriveUnMounted(const SString &mountLocation) :
      CEvent(EventLevel::Normal),
      _mountLocation(mountLocation) {
    }

  private:
    SiaDriveUnMounted() :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~SiaDriveUnMounted() {
    }

  private:
    SString _mountLocation;

  protected:
    virtual void LoadData(const json& j) override {
      _mountLocation = j["mount_location"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaDriveUnMounted(_mountLocation));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|LOC|" + _mountLocation;
    }

    virtual json GetEventJson() const override {
      return {{"event",          GetEventName()},
              {"mount_location", _mountLocation}};
    }
END_EVENT(SiaDriveUnMounted);

BEGIN_EVENT(SiaDriveMounted)
  public:
    SiaDriveMounted(const SString &mountLocation) :
      CEvent(EventLevel::Normal),
      _mountLocation(mountLocation) {
    }

  private:
    SiaDriveMounted() :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~SiaDriveMounted() {
    }

  private:
    SString _mountLocation;

  protected:
    virtual void LoadData(const json& j) override {
      _mountLocation = j["mount_location"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaDriveMounted(_mountLocation));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|LOC|" + _mountLocation;
    }

    virtual json GetEventJson() const override {
      return {{"event",          GetEventName()},
              {"mount_location", _mountLocation}};
    }
END_EVENT(SiaDriveMounted);

BEGIN_EVENT(SiaDriveCacheEviction)
  public:
    SiaDriveCacheEviction(const SString &siaPath, const FilePath &filePath, const bool& cacheSizeEviction, const double& redundancy) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath),
      _policy(cacheSizeEviction ? "size" : "redundancy"),
      _redundancy(redundancy) {
    }

    SiaDriveCacheEviction(const SString &siaPath, const FilePath &filePath, const SString& policy, const double& redundancy) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath),
      _policy(policy),
      _redundancy(redundancy) {
    }

  private:
    SiaDriveCacheEviction() :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~SiaDriveCacheEviction() {
    }

  private:
    SString _siaPath;
    FilePath _filePath;
    SString _policy;
    double _redundancy;

  protected:
    virtual void LoadData(const json& j) override {
      _filePath = FilePath(j["file_path"].get<std::string>());
      _siaPath = j["sia_path"].get<std::string>();
      _policy = j["policy"].get<std::string>();
      _redundancy = j["redundancy"].get<double>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath + "|POLICY|" + _policy + "|" + SString::FromDouble(_redundancy);
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveCacheEviction>(_siaPath, _filePath, _policy, _redundancy));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"file_path", static_cast<SString>(_filePath)},
              {"sia_path",  _siaPath},
              {"policy", _policy},
              {"redundancy", _redundancy}};
    }
END_EVENT(SiaDriveCacheEviction);

BEGIN_EVENT(SiaDriveCacheFull)
  public:
    SiaDriveCacheFull(const SString &cacheLocation) :
      CEvent(EventLevel::Error),
      _cacheLocation(cacheLocation) {
    }

  private:
    SiaDriveCacheFull() :
      CEvent(EventLevel::Error) {
    }

  public:
    virtual ~SiaDriveCacheFull() {
    }

  private:
    SString _cacheLocation;

  protected:
    virtual void LoadData(const json& j) override {
      _cacheLocation = j["cache_location"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaDriveCacheFull(_cacheLocation));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|LOC|" + _cacheLocation;
    }

    virtual json GetEventJson() const override {
      return {{"event",          GetEventName()},
              {"cache_location", _cacheLocation}};
    }
END_EVENT(SiaDriveCacheFull);

BEGIN_EVENT(SiaOnline)
  public:
    SiaOnline()  :
      CEvent(EventLevel::Normal) {
    }

  protected:
    virtual void LoadData(const json& j) override {
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName();
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::static_pointer_cast<CEvent>(std::make_shared<SiaOnline>());
    }

    virtual json GetEventJson() const override {
      return {{"event",  GetEventName()}};
    }
END_EVENT(SiaOnline);

BEGIN_EVENT(SiaOffline)
  public:
    SiaOffline()  :
      CEvent(EventLevel::Normal) {
    }

  protected:
    virtual void LoadData(const json& j) override {
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName();
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::static_pointer_cast<CEvent>(std::make_shared<SiaOffline>());
    }

    virtual json GetEventJson() const override {
      return {{"event",  GetEventName()}};
    }
END_EVENT(SiaOffline);


CEventPtr CreateSystemCriticalEvent(const SString &reason) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SystemCriticalEvent>(reason));
}

CEventPtr CreateFileClosedEvent(const SString &siaPath, const FilePath &filePath, const bool &wasChanged) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveFileClosed>(siaPath, filePath, wasChanged));
}

CEventPtr CreateDownloadToCacheBeginEvent(const SString &siaPath, const FilePath &filePath, const FilePath &tempFilePath) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveDownloadToCacheBegin>(siaPath, filePath, tempFilePath));
}

CEventPtr CreateDownloadToCacheEndEvent(const SString &siaPath, const FilePath &filePath, const FilePath &tempFilePath, const SiaApiError &result) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveDownloadToCacheEnd>(siaPath, filePath, tempFilePath, result));
}

CEventPtr CreateCacheFileAddedEvent(const SString &siaPath, const FilePath &filePath, const bool &succeeded, const SString &reason) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveCacheFileAdded>(siaPath, filePath, succeeded, reason));
}

CEventPtr CreateFileUnrecoverableEvent(const SString &siaPath, const FilePath &filePath) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveFileUnrecoverable>(siaPath, filePath));
}

CEventPtr CreateMountEndedEvent(const SString &mountLocation, const std::int32_t &result) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveMountEnded>(mountLocation, result));
}

CEventPtr CreateMountedEvent(const SString &mountLocation) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveMounted>(mountLocation));
}

CEventPtr CreateUnMountedEvent(const SString &mountLocation) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveUnMounted>(mountLocation));
}

CEventPtr CreateCacheEvictionEvent(const SString &siaPath, const FilePath &filePath, const bool& cacheSizeEviction, const double& redundancy) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveCacheEviction>(siaPath, filePath, cacheSizeEviction, redundancy));
}

CEventPtr CreateCacheFullEvent(const SString& cacheLocation) {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaDriveCacheFull>(cacheLocation));
}

CEventPtr CreateSiaOnlineEvent() {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaOnline>());
}

CEventPtr CreateSiaOfflineEvent() {
  return std::static_pointer_cast<CEvent>(std::make_shared<SiaOffline>());
}
NS_END(2)
