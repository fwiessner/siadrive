#include <siatransfermanager.h>
#include <siaapi.h>
#include <siadriveconfig.h>
#include <eventsystem.h>
#include <vfs/itemlock.h>
#include <SQLiteCpp/Database.h>
#include <filepath.h>

using namespace Sia::Api;

BEGIN_EVENT(TransferManagerError)
  public:
    TransferManagerError(const SString &func, const SQLite::Exception &exception) :
      CEvent(EventLevel::Error),
      _func(func),
      _reason(exception.what()) {
    }

    TransferManagerError(const SString &func, const SString &reason) :
      CEvent(EventLevel::Error),
      _func(func),
      _reason(reason) {
    }

  private:
    TransferManagerError() :
      CEvent(EventLevel::Error) {
    }

  public:
    virtual ~TransferManagerError() {
    }

  private:
    SString _func;
    SString _reason;

  protected:
    virtual void LoadData(const json& j) override {
      _func = j["function_name"].get<std::string>();
      _reason = j["reason"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new TransferManagerError(_func, _reason));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|FUNC|" + _func + "|REASON|" + _reason;
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"function_name",   _func},
              {"reason", _reason}};
    }
END_EVENT(TransferManagerError);

BEGIN_EVENT(ExternallyRemovedFileDetected)
  public:
    ExternallyRemovedFileDetected(const SString &siaPath) :
      CEvent(EventLevel::Warn),
      _siaPath(siaPath) {
    }

  public:
    virtual ~ExternallyRemovedFileDetected() {
    }

  private:
    ExternallyRemovedFileDetected()  :
      CEvent(EventLevel::Warn) {
    }

  private:
    SString _siaPath;

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new ExternallyRemovedFileDetected(_siaPath));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"sia_path",  _siaPath}};
    }
END_EVENT(ExternallyRemovedFileDetected);

BEGIN_EVENT(FileAddedToQueue)
  public:
    FileAddedToQueue(const SString &siaPath, const SString &filePath) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath) {
    }

  private:
    FileAddedToQueue() :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~FileAddedToQueue() {
    }

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
      _filePath = j["file_path"].get<std::string>();
    }

  private:
    SString _siaPath;
    SString _filePath;

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new FileAddedToQueue(_siaPath, _filePath));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"sia_path",  _siaPath},
              {"file_path", _filePath}};
    }
END_EVENT(FileAddedToQueue);

BEGIN_EVENT(FileRemovedFromSia)
  public:
    FileRemovedFromSia(const SString &siaPath) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath) {
    }

  private:
    FileRemovedFromSia()  :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~FileRemovedFromSia() {
    }

  private:
    SString _siaPath;

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new FileRemovedFromSia(_siaPath));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"sia_path",  _siaPath}};
    }
END_EVENT(FileRemovedFromSia);

BEGIN_EVENT(FailedToQueueUpload)
  public:
    FailedToQueueUpload(const SString &siaPath, const SString &sourcePath, const SString &errorMessage) :
      CEvent(EventLevel::Error),
      _siaPath(siaPath),
      _sourcePath(sourcePath),
      _errorMsg(errorMessage) {
    }

  private:
    FailedToQueueUpload()  :
      CEvent(EventLevel::Error) {
    }

  public:
    virtual ~FailedToQueueUpload() {
    }

  private:
    SString _siaPath;
    SString _sourcePath;
    SString _errorMsg;

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
      _sourcePath = j["file_path"].get<std::string>();
      _errorMsg = j["message"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _sourcePath + "|MSG|" + _errorMsg;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new FailedToQueueUpload(_siaPath, _sourcePath, _errorMsg));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"sia_path",  _siaPath},
              {"file_path", _sourcePath},
              {"message",   _errorMsg}};
    }
END_EVENT(FailedToQueueUpload);

BEGIN_EVENT(FailedToRemoveFileFromSia)
  public:
    FailedToRemoveFileFromSia(const SString &siaPath, const SiaCommError &curlError) :
      CEvent(EventLevel::Error),
      _siaPath(siaPath),
      _curlError(curlError.GetReason()) {
    }

  private:
    FailedToRemoveFileFromSia(const SString &siaPath, const SString &curlError) :
      CEvent(EventLevel::Error),
      _siaPath(siaPath),
      _curlError(curlError) {
    }

  private:
    FailedToRemoveFileFromSia()  :
      CEvent(EventLevel::Error) {
    }

  public:
    virtual ~FailedToRemoveFileFromSia() {
    }

  private:
    SString _siaPath;
    SString _curlError;

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
      _curlError = j["curl_error"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|CERR|" + _curlError;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new FailedToRemoveFileFromSia(_siaPath, _curlError));
    }

    virtual json GetEventJson() const override {
      return {{"event",      GetEventName()},
              {"sia_path",   _siaPath},
              {"curl_error", _curlError}};
    }
END_EVENT(FailedToRemoveFileFromSia);

BEGIN_EVENT(SourceFileNotFound)
  public:
    SourceFileNotFound(const SString &siaPath, const SString &filePath) :
      CEvent(EventLevel::Error),
      _siaPath(siaPath),
      _filePath(filePath) {
    }

  private:
    SourceFileNotFound()  :
      CEvent(EventLevel::Error) {
    }

  public:
    virtual ~SourceFileNotFound() {
    }

  private:
    SString _siaPath;
    SString _filePath;

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
      _filePath = j["file_path"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SourceFileNotFound(_siaPath, _filePath));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath;
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"sia_path",  _siaPath},
              {"file_path", _filePath}};
    }
END_EVENT(SourceFileNotFound);

BEGIN_EVENT(UploadToSiaStarted)
  public:
    UploadToSiaStarted(const SString &siaPath, const SString &filePath) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath),
      _filePath(filePath) {
    }

  private:
    UploadToSiaStarted()  :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~UploadToSiaStarted() {
    }

  private:
    SString _siaPath;
    SString _filePath;

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
      _filePath = j["file_path"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath + "|FP|" + _filePath;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new UploadToSiaStarted(_siaPath, _filePath));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"sia_path",  _siaPath},
              {"file_path", _filePath}};
    }
END_EVENT(UploadToSiaStarted);

BEGIN_EVENT(UploadToSiaComplete)
  public:
    UploadToSiaComplete(const SString &siaPath) :
      CEvent(EventLevel::Normal),
      _siaPath(siaPath) {
    }

  private:
    UploadToSiaComplete()  :
      CEvent(EventLevel::Normal) {
    }

  public:
    virtual ~UploadToSiaComplete() {
    }

  private:
    SString _siaPath;

  protected:
    virtual void LoadData(const json& j) override {
      _siaPath = j["sia_path"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|SP|" + _siaPath;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new UploadToSiaComplete(_siaPath));
    }

    virtual json GetEventJson() const override {
      return {{"event",     GetEventName()},
              {"sia_path",  _siaPath}};
    }
END_EVENT(UploadToSiaComplete);


CSiaTransferManager::CSiaTransferManager(std::shared_ptr<CSiaApi> siaApi) :
  CAutoThread(siaApi->GetSiaDriveConfig()),
  _siaApi(siaApi),
  _database(siaApi->GetSiaDriveConfig()->GetRenter_TransferDbFilePath(), SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE) {
  const auto sql = "create if not exists transfer_table (id integer primary key autoincrement, path text unique not null, source text unique not null, status integer not null);";
  SQLite::Statement createStmt(_database, sql);
  createStmt.exec();
}

CSiaTransferManager::~CSiaTransferManager() {
  Stop();
  _siaApi.reset();
}

void CSiaTransferManager::AutoThreadCallback(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) {
  try {
    CSiaFileTreePtr fileTree;
    if (ApiSuccess(_siaApi->GetRenter().GetFileTree(fileTree))) {
      std::lock_guard<std::mutex> l(_dbMutex);
      auto activeCount = ProcessUploadsByStatus(UploadStatus::Uploading, fileTree);
      ProcessQueuedByStatus(UploadStatus::Queued, activeCount) &&
        ProcessQueuedByStatus(UploadStatus::Retry, activeCount);
    }
  } catch (const SQLite::Exception& e) {
    CEventSystem::EventSystem.NotifyEvent(std::make_shared<TransferManagerError>(__FUNCTION__, e));
  }
}

bool CSiaTransferManager::Download(const SString &path, const SString& destPath) {
  const auto err = _siaApi->GetRenter().DownloadFile(path, destPath, _isActive);
  return ApiSuccess(err);
}

bool CSiaTransferManager::ProcessQueuedByStatus(const UploadStatus& uploadStatus, std::int32_t& activeCount) {
  SQLite::Statement queuedStmt(_database, "select * as upload_count from transfer_table where status=@status");
  queuedStmt.bind("@status", (int) uploadStatus);
  while (((_siaApi->GetSiaDriveConfig()->GetMaxUploadCount() <= -1) || (activeCount < _siaApi->GetSiaDriveConfig()->GetMaxUploadCount())) &&
    queuedStmt.executeStep()) {
    const SString path = queuedStmt.getColumn("path").getString();
    const SString source = queuedStmt.getColumn("source").getString();
    if (FilePath(source).IsFile()) {
      json response;
      const auto err = _siaApi->GetSiaDriveConfig()->GetSiaCommunicator()->Post(SString("/renter/upload") + path, { { "source", source } }, response);
      const bool alreadyExists = err.GetReason().Contains("already exists");
      if (ApiSuccess(err) || alreadyExists) {
        if (not alreadyExists) {
          activeCount++;
        }
        SetUploadStatus(path, UploadStatus::Uploading);
        if (not alreadyExists) {
          CEventSystem::EventSystem.NotifyEvent(std::make_shared<UploadToSiaStarted>(path, source));
        }
      }
      else {
        CEventSystem::EventSystem.NotifyEvent(std::make_shared<TransferManagerError>(__FUNCTION__, err.GetReason()));
        if (uploadStatus != UploadStatus::Retry) {
          SetUploadStatus(path, UploadStatus::Retry);
        }
      }
    }
    else {
      RemoveFromDatabase(path);
      CEventSystem::EventSystem.NotifyEvent(std::make_shared<SourceFileNotFound>(path, source));
    }
  }

  return ((_siaApi->GetSiaDriveConfig()->GetMaxUploadCount() <= -1) ||
          (activeCount < _siaApi->GetSiaDriveConfig()->GetMaxUploadCount()));
}

std::int32_t CSiaTransferManager::ProcessUploadsByStatus(const UploadStatus& uploadStatus, CSiaFileTreePtr fileTree) {
  SQLite::Statement uploadsStmt(_database, "select * as upload_count from transfer_table where status=@status");
  uploadsStmt.bind("@status", (int) uploadStatus);
  std::int32_t activeCount = 0;
  while (uploadsStmt.executeStep()) {
    const SString path = uploadsStmt.getColumn("path").getString();
    const auto &fileList = fileTree->GetFileList();
    auto it = std::find_if(fileList.begin(), fileList.end(), [&](const CSiaFilePtr &ptr) {
      return ptr->GetSiaPath() == path;
    });

    // Removed by another client
    if (it == fileList.end()) {
      CEventSystem::EventSystem.NotifyEvent(std::make_shared<ExternallyRemovedFileDetected>(path));
      Remove(path);
    }
      // Upload is complete
    else if ((*it)->GetAvailable()) {
      RemoveFromDatabase(path);
      CEventSystem::EventSystem.NotifyEvent(std::make_shared<UploadToSiaComplete>(path));
    } else {
      activeCount++;
    }
  }

  return activeCount;
}

bool CSiaTransferManager::Remove(const SString &path) {
  bool ret = false;
  try {
    std::lock_guard<std::mutex> l(_dbMutex);
    const auto deleteCount = RemoveFromDatabase(path);

    json response;
    const auto err = _siaApi->GetSiaDriveConfig()->GetSiaCommunicator()->Post(SString("/renter/delete") + path, {}, response);
    const bool notFound = err.GetReason().Contains("no file known");
    ret = (ApiSuccess(err) || notFound);
    if (not notFound) {
      if (ret) {
        if (deleteCount > 0) {
          CEventSystem::EventSystem.NotifyEvent(std::make_shared<FileRemovedFromSia>(path));
        }
      } else {
        CEventSystem::EventSystem.NotifyEvent(std::make_shared<FailedToRemoveFileFromSia>(path, err));
      }
    }
  } catch (const SQLite::Exception& e) {
    CEventSystem::EventSystem.NotifyEvent(std::make_shared<TransferManagerError>(__FUNCTION__, e));
  }
  
  return ret;
}

int CSiaTransferManager::RemoveFromDatabase(const SString& path) {
  SQLite::Statement deleteStmt(_database, "delete from transfer_table where path=@path;");
  deleteStmt.bind("@path", &path[0]);
  return deleteStmt.exec();
}

void CSiaTransferManager::SetUploadStatus(const SString& path, const UploadStatus& uploadStatus) {
  SQLite::Statement updateStmt(_database, "update transfer_table set status=@status where path=@path;");
  updateStmt.bind("@path", &path[0]);
  updateStmt.bind("@status", (int)uploadStatus);
  updateStmt.exec();
}

void CSiaTransferManager::Start() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  if (not _isActive) {
    _isActive = true;
    StartAutoThread();
  }
}

void CSiaTransferManager::Stop() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  if (_isActive) {
    _isActive = false;
    StopAutoThread();
  }
}

bool CSiaTransferManager::Upload(const SString &path, const SString &sourcePath) {
  bool ret = false;
  try {
    std::lock_guard<std::mutex> l(_dbMutex);
    SQLite::Statement insertStmt(_database, "insert into transfer_table (path, source, status) values (@path, @source, @status)");
    insertStmt.bind("@path", &path[0]);
    insertStmt.bind("@source", &sourcePath[0]);
    insertStmt.bind("@status", (int)UploadStatus::Queued);
    insertStmt.exec();
    CEventSystem::EventSystem.NotifyEvent(std::make_shared<FileAddedToQueue>(path, sourcePath));
    ret = true;
  } catch (const SQLite::Exception& e) {
    CEventSystem::EventSystem.NotifyEvent(std::make_shared<TransferManagerError>(__FUNCTION__, e));
  }

  return ret;
}