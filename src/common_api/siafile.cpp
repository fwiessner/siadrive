#include <siaapi.h>

#include <utility>

using namespace Sia::Api;

CSiaApi::_CSiaFile::_CSiaFile(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, const json &fileJson) :
  CSiaBase(std::move(siaDriveConfig)),
  _SiaPath(fileJson["siapath"].get<std::string>()),
  _FileSize(fileJson["filesize"].get<std::uint64_t>()),
  _Available(fileJson["available"].get<bool>()),
  _Renewing(fileJson["renewing"].get<bool>()),
  _Redundancy(fileJson["redundancy"].get<double>()),
  _UploadProgress(fileJson["uploadprogress"].get<std::uint32_t>()),
  _Expiration(fileJson["expiration"].get<std::uint32_t>()) {
}

CSiaApi::_CSiaFile::~_CSiaFile() = default;
