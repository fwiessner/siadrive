#include <siaapi.h>
#include <siadriveconfig.h>

#include <utility>
#include <eventsystem.h>

using namespace Sia::Api;

static SString SeedLangToString(const SiaSeedLanguage &lang) {
  switch (lang) {
  case SiaSeedLanguage::English:
    return "english";
  case SiaSeedLanguage::German:
    return "german";
  case SiaSeedLanguage::Japanese:
    return "japanese";
  }
}

CSiaApi::_CSiaWallet::_CSiaWallet(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  CSiaBase(std::move(siaDriveConfig)),
  _Created(false),
  _Locked(false),
  _Connected(false),
  _ConfirmedBalance(0),
  _UnconfirmedBalance(0) {
}

CSiaApi::_CSiaWallet::~_CSiaWallet() = default;

/*{
"encrypted": true,
"unlocked":  true,

"confirmedsiacoinbalance":     "123456", // hastings, big int
"unconfirmedoutgoingsiacoins": "0",      // hastings, big int
"unconfirmedincomingsiacoins": "789",    // hastings, big int

"siafundbalance":      "1",    // siafunds, big int
"siacoinclaimbalance": "9001", // hastings, big int
}*/
void CSiaApi::_CSiaWallet::Refresh() {
  bool connected = false;
  bool locked = false;
  bool created = false;
  SiaCurrency unconfirmed = 0;
  SiaCurrency confirmed = 0;
  SString address;
  {
    json walletResult;
    SiaCommError cerror = GetSiaDriveConfig()->GetSiaCommunicator()->Get("/wallet", walletResult);
    if (ApiSuccess(cerror)) {
      connected = true;
      created = walletResult["encrypted"].get<bool>();
      locked = not walletResult["unlocked"].get<bool>();
      confirmed = HastingsStringToSiaCurrency(walletResult["confirmedsiacoinbalance"].get<std::string>());
      SiaCurrency sc1 = HastingsStringToSiaCurrency(walletResult["unconfirmedincomingsiacoins"].get<std::string>());
      SiaCurrency sc2 = HastingsStringToSiaCurrency(walletResult["unconfirmedoutgoingsiacoins"].get<std::string>());
      unconfirmed = sc1 - sc2;
      if (GetReceiveAddress().IsNullOrEmpty()) {
        json walletAddrResult;
        cerror = GetSiaDriveConfig()->GetSiaCommunicator()->Get("/wallet/address", walletAddrResult);
        address = ApiSuccess(cerror) ? walletAddrResult["address"].get<std::string>() : "";
      }
    }
  }
  SetCreated(created);
  SetLocked(locked);
  SetConfirmedBalance(confirmed);
  SetUnconfirmedBalance(unconfirmed);
  SetReceiveAddress(address);

  // Update connected status last so all properties are current
  SetConnected(connected);
  if (connected) {
    CEventSystem::EventSystem.NotifyEvent(CreateSiaOnlineEvent());
  } else {
    CEventSystem::EventSystem.NotifyEvent(CreateSiaOfflineEvent());
  }
}

SiaApiError CSiaApi::_CSiaWallet::Create(const SString &password, const SiaSeedLanguage &seedLanguage, SString &seed) {
  SiaApiError error = SiaApiErrorCode::NotConnected;
  if (GetConnected()) {
    if (GetCreated()) {
      error = SiaApiErrorCode::WalletExists;
    } else {
      json result;
      SiaCommError cerror = password.IsNullOrEmpty() ? GetSiaDriveConfig()->GetSiaCommunicator()->Post("/wallet/init", {{"dictionary", SeedLangToString(seedLanguage)}}, result) : GetSiaDriveConfig()->GetSiaCommunicator()->Post("/wallet/init", {{"dictionary",         SeedLangToString(seedLanguage)},
                                                                                                                                                                                                                                                     {"encryptionpassword", password}}, result);
      if (ApiSuccess(cerror)) {
        error = SiaApiErrorCode::Success;
        seed = result["primaryseed"].get<std::string>();
        SetCreated(true);
        SetLocked(true);
      } else {
        error = {SiaApiErrorCode::RequestError,
                 cerror.GetReason()};
      }
    }
  }
  return error;
}

void CSiaApi::_CSiaWallet::ResetReceiveAddress() {
  SetReceiveAddress("");
}

SiaApiError CSiaApi::_CSiaWallet::Restore(const SString &seed) {
  SiaApiError error = SiaApiErrorCode::NotImplemented;
  // TODO Future enhancement
  return error;
}

SiaApiError CSiaApi::_CSiaWallet::Lock() {
  SiaApiError error = GetCreated() ? (GetLocked() ? SiaApiErrorCode::WalletLocked : SiaApiErrorCode::Success) : SiaApiErrorCode::WalletNotCreated;
  if (ApiSuccess(error)) {
    json result;
    SiaCommError cerror = GetSiaDriveConfig()->GetSiaCommunicator()->Post("/wallet/lock", {}, result);
    if (ApiSuccess(cerror)) {
      error = SiaApiErrorCode::Success;
    } else {
      error = {SiaApiErrorCode::RequestError,
               cerror.GetReason()};
    }
  }
  return error;
}

SiaApiError CSiaApi::_CSiaWallet::Unlock(const SString &password) {
  SiaApiError error = GetCreated() ? (GetLocked() ? SiaApiErrorCode::Success : SiaApiErrorCode::WalletUnlocked) : SiaApiErrorCode::WalletNotCreated;
  if (ApiSuccess(error)) {
    json result;
    SiaCommError cerror = GetSiaDriveConfig()->GetSiaCommunicator()->Post("/wallet/unlock", {{"encryptionpassword", password}}, result);
    if (ApiSuccess(cerror)) {
      error = SiaApiErrorCode::Success;
    } else {
      error = {SiaApiErrorCode::RequestError,
               cerror.GetReason()};
    }
  }
  return error;
}

SiaApiError CSiaApi::_CSiaWallet::Send(const SString &address, const SiaCurrency &amount) {
  SiaApiError error = SiaApiErrorCode::Success;
  const SString hast = SiaCurrencyToHastingsString(amount);
  json result;
  SiaCommError cerror = GetSiaDriveConfig()->GetSiaCommunicator()->Post("/wallet/siacoins", {{"destination", address},
                                                                                              {"amount",      hast}}, result);
  if (ApiSuccess(cerror)) {
    error = SiaApiErrorCode::Success;
  } else {
    error = {SiaApiErrorCode::RequestError,
             cerror.GetReason()};
  }
  return error;
}
