#include <siacommon.h>
#include <SQLiteCpp/Exception.h>
#include <sqlite3.h>
#include <filepath.h>
#include <siaapi.h>
#include <sys/stat.h>
#include <picosha2.h>

#ifdef _WIN32
#include <bitset>

#else

#include <fstream>

#endif

NS_BEGIN(Sia)
NS_BEGIN(Api)
SString GenerateSha256(const SString &str) {
  std::string ret;
  picosha2::hash256_hex_string(str, ret);
  return ret;
}

BOOL RetryAction(std::function<BOOL()> func, std::uint16_t retryCount, const DWORD &retryDelay) {
  BOOL ret = FALSE;
  while (retryCount-- && not((ret = func()))) {
    std::this_thread::sleep_for(std::chrono::milliseconds(retryDelay));
  }
  return ret;
}

SString BytesToFriendlyDisplay(const SiaCurrency &bytes) {
  SString units[] = {"B",
                     "KB",
                     "MB",
                     "GB",
                     "TB",
                     "PB"};
  SString readableunit = "B";
  SiaCurrency readablesize = bytes;
  for (const auto &unit : units) {
    if (readablesize < 1000) {
      readableunit = unit;
      break;
    }
    readablesize /= 1000;
  }
  ttmath::Conv conv;
  conv.scient_from = 256;
  conv.base = 10;
  conv.round = 2;
  return readablesize.ToWString(conv) + ' ' + readableunit;
}

#ifdef _WIN32

std::vector<SString> GetAvailableDrives() {
  static const std::vector<char> alpha = {'A',
                                          'B',
                                          'C',
                                          'D',
                                          'E',
                                          'F',
                                          'G',
                                          'H',
                                          'I',
                                          'J',
                                          'K',
                                          'L',
                                          'M',
                                          'N',
                                          'O',
                                          'P',
                                          'Q',
                                          'R',
                                          'S',
                                          'T',
                                          'U',
                                          'V',
                                          'W',
                                          'X',
                                          'Y',
                                          'Z'};
  std::bitset<26> drives(~GetLogicalDrives() & 0xFFFFFFFF);
  std::vector<SString> avail;
  for (size_t i = 0; i < alpha.size(); i++) {
    if (drives[i] && (alpha[i] != 'A') && (alpha[i] != 'B')) {
      avail.push_back(alpha[i]);
    }
  }
  return std::move(avail);
}

std::int32_t GetRegistry(const SString &name, const std::int32_t &defaultValue, const bool &user) {
  std::int32_t ret = defaultValue;
  DWORD dataLen = 0;
  if (::RegGetValue((user ? HKEY_CURRENT_USER : HKEY_LOCAL_MACHINE), "Software\\SiaExtensions\\SiaDrive", &name[0], RRF_RT_REG_SZ, nullptr, nullptr, &dataLen) == ERROR_SUCCESS) {
    SString data;
    data.Resize(dataLen + 1);
    if (::RegGetValue((user ? HKEY_CURRENT_USER : HKEY_LOCAL_MACHINE), "Software\\SiaExtensions\\SiaDrive", &name[0], RRF_RT_REG_SZ, nullptr, &data[0], &dataLen) == ERROR_SUCCESS) {
      ret = SString::ToInt32(data);
    }
  }
  return ret;
}

SString GetRegistry(const SString &name, const SString &defaultValue, const bool &user) {
  SString ret = defaultValue;
  DWORD dataLen = 0;
  if (::RegGetValue((user ? HKEY_CURRENT_USER : HKEY_LOCAL_MACHINE), "Software\\SiaExtensions\\SiaDrive", &name[0], RRF_RT_REG_SZ, nullptr, nullptr, &dataLen) == ERROR_SUCCESS) {
    SString data;
    data.Resize(dataLen + 1);
    if (::RegGetValue((user ? HKEY_CURRENT_USER : HKEY_LOCAL_MACHINE), "Software\\SiaExtensions\\SiaDrive", &name[0], RRF_RT_REG_SZ, nullptr, &data[0], &dataLen) == ERROR_SUCCESS) {
      ret = data;
    }
  }
  return ret;
}

void SetRegistry(const SString &name, const SString &value) {
  ::RegSetKeyValue(HKEY_CURRENT_USER, "Software\\SiaExtensions\\SiaDrive", &name[0], REG_SZ, &value[0], value.ByteLength() + sizeof(SString::SChar));
}

ComInitWrapper::ComInitWrapper() :
  _uninit(SUCCEEDED(::CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED))) {
}

ComInitWrapper::~ComInitWrapper() {
  if (_uninit) {
    ::CoUninitialize();
  }
}
#endif
NS_END(2)
