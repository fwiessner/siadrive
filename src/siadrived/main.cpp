#include <siacommon.h>
#include <siaapi.h>
#include <platform.h>
#include <siadriveconfig.h>
#include <eventsystem.h>
#include <loggingconsumer.h>
#include <debugconsumer.h>
#include "privatelistener.h"
#include "publiclistener.h"
#include <siatransfermanager.h>
#ifndef _WIN32
#include <unistd.h>
#include <csignal>
#include <sys/stat.h>
#endif

using namespace Sia::Api;
using namespace std::chrono_literals;

static std::condition_variable _stopVar;
static std::mutex _stopMutex;
bool _shutdownRequested = false;

#ifndef _WIN32
static void ShutdownHandler(int sig) {
  if (not _shutdownRequested) {
    // Synchronize to prevent invalid states
    //  (i.e. critical exception during startup)
    {
      std::unique_lock<std::mutex> l(_stopMutex);
      _shutdownRequested = true;
    }
    _stopVar.notify_all();
  }
}
#endif

int main(int argc, char** argv) {
  // --background            - background daemon
  // --private-port=<port #> - private drive api port
  // --public-port=<port #>  - public drive api port
  // --help                  - display usage/help
  // --version               - display version info

  const auto background = false;
  if (background) {
#ifndef _WIN32
    errno = 0;
    const auto forkRes = fork();
    if ((forkRes != 0) || (setsid() < 0) || (setpgid(0,0) < 0)) {
      exit(errno);
    }
    umask(0);
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);
    signal(SIGINT, ShutdownHandler);
    signal(SIGTERM, ShutdownHandler);
    signal(SIGQUIT, ShutdownHandler);
#endif
  }
 
  // Only allow one instance of this process
  if (PlatformCheckSingleInstance(GenerateSha256("siadrived_5b07b5ea-5906-46a2-8fe6-a8309c237b32"))) {
    // Keep as shared_ptr<> to decrease threading synchronization. May revisit
    //  later if performance ever becomes an issue.
    //  Allows destruction of objects when all references have been removed
    auto siaComm = std::shared_ptr<ISiaApiCommunicator>(new CSiaCurl());
    auto siaDriveConfig = std::make_shared<CSiaDriveConfig>(true, siaComm);

    // Configure and start event system
#ifdef _DEBUG
    CDebugConsumer debugConsumer;
#endif
    CLoggingConsumer loggingConsumer(siaDriveConfig->GetLogDirectory(), EventLevelFromString(siaDriveConfig->GetEventLevel()));
    
    // Handle critical events by shutting down
    CEventSystem::EventSystem.AddEventConsumer([](const CEvent& e) {
      if (e.GetEventName() == "SystemCriticalEvent") {
        ShutdownHandler(SIGTERM);
      }
    });
    
    CEventSystem::EventSystem.Start();

    try {
      auto siaApi = std::make_shared<CSiaApi>(siaDriveConfig, true);

      // Start 'siad' if allowed
      //  Control is allowed only if this process launches 'siad'. If 'siad'
      //  is already running, then we don't want to assume ownership of termination
      //  as this may be undesired behavior. See 'siadriveconfig.json' or CSiaDriveConfig
      //  for available options.
      siaApi->StartSiad();

      // Create public and private Drive API
      CPublicListener publicListener(siaApi);
      CPrivateListener privateListener(siaApi);

      // Start API refresh
      siaApi->StartBackgroundRefresh();

      // Start public and private Drive API
      publicListener.Start();
      privateListener.Start();

      // Wait for termination
      {
        std::unique_lock<std::mutex> l(_stopMutex);
        // May already be 'true' if bad, bad stuff happened on startup - so don't wait
        while (not _shutdownRequested) {
          _stopVar.wait_for(l, 2s);
        }
      }

      // Stop public and private Drive API
      publicListener.Stop();
      privateListener.Stop();

      // Stop API refresh
      siaApi->StopBackgroundRefresh();

      // Stop 'siad' if allowed
      //  This will only work if this process launched the running 'siad'
      siaApi->StopSiad();
    } catch (const std::exception& e) {
      CEventSystem::EventSystem.NotifyEvent(CreateSystemCriticalEvent(e.what()));
    }

    // Allow new process to start
    PlatformReleaseSingleInstance();
  }

  // Always stop event system as last item before exit
  CEventSystem::EventSystem.Stop();
  return 0;
}