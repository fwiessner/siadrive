#ifndef _SIACURL_H
#define _SIACURL_H
#include <siacommon.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)
class SIADRIVE_EXPORTABLE CSiaCurl :
  public virtual ISiaApiCommunicator {
  public:
    CSiaCurl();

  public:
    virtual ~CSiaCurl();

  public:
    static SString UrlEncode(const SString &data, const bool &allowSlash = false);

  private:
    static SString GetApiErrorMessage(SString result);

  private:
    std::string ConstructPath(const SString &relativePath) const;
    SiaCommError _Get(const SString &path, const HttpParameters &parameters, json &response) const;
    bool CheckVersion(SiaCommError &error) const;
    SiaCommError ProcessResponse(const int &res, const int &httpCode, const std::string &result, json &response) const;

  public:
    virtual SString GetServerVersion() const override;
    virtual SiaCommError Get(const SString &path, json &result) const override;
    virtual SiaCommError Get(const SString &path, const HttpParameters &parameters, json &result) const override;
    virtual SiaCommError Post(const SString &path, const HttpParameters &parameters, json &response) const override;
};
NS_END(2)
#endif //_SIACURL_H