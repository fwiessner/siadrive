#ifndef _SIAVFS_H
#define _SIAVFS_H
#include <siacommon.h>
#include <siatransfermanager.h>
#include <vfs/sqliteitemstore.h>
#include <vfs/virtualfilesystem.h>
#include <vfs/vfsitem.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

#ifdef _WIN32
typedef struct {
  SECURITY_ATTRIBUTES SecAttr;
} AccessData;

typedef struct {
  DWORD CreateDisp;
  DWORD DesiredAccess;
  DWORD Flags;
  DWORD ShareMode;
} OpenData;

#define VFS_ACCESS_CHECKER [](const SString& path, const bool& directory, AccessData accessData, const OpenData& openData, const bool& created)->bool {\
  return true;\
}

#define VFS_CREATOR [](const SString& sourcePath, const bool& directory, AccessData accessData, const OpenData& openData, HANDLE& handle)->bool { \
  handle = ::CreateFile(&sourcePath[0], openData.DesiredAccess, openData.ShareMode, accessData.SecAttr.nLength ? &accessData.SecAttr : nullptr, openData.CreateDisp, openData.Flags, nullptr);\
  return handle != VFSInvalidHandle;\
}

#define VFS_CLOSER [](HANDLE& handle)->bool { \
  bool ret = ::CloseHandle(handle) ? true : false;\
  handle = VFSInvalidHandle;\
  return ret;\
}

#else
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

typedef struct {
  gid_t GID;
  mode_t Mode;
  uid_t UID;
} AccessData;

typedef struct {
  int Flags;
} OpenData;

enum class Perm {
    Execute,
    Read,
    Write
};

enum class AccessLevel {
    Group,
    Other,
    User
};

namespace VFSUtils {
inline bool CheckOpenFlags(const std::int32_t &acc, const bool &write) {
  return (write && (acc & O_WRONLY)) || (not write && (acc & O_RDONLY)) || (acc & O_RDWR);
}

bool CheckPerm(const Perm &perm, const AccessLevel &accessLevel, const mode_t &mode) {
  switch (perm) {
  case Perm::Execute: {
    switch (accessLevel) {
    case AccessLevel::Group: {
      return mode & S_IXGRP;
    }
    case AccessLevel::Other: {
      return mode & S_IXOTH;
    }
    case AccessLevel::User: {
      return mode & S_IXUSR;
    }
    }
  }

  case Perm::Read : {
    switch (accessLevel) {
    case AccessLevel::Group: {
      return mode & S_IRGRP;
    }
    case AccessLevel::Other: {
      return mode & S_IROTH;
    }
    case AccessLevel::User: {
      return mode & S_IRUSR;
    }
    }
  }

  case Perm::Write: {
    switch (accessLevel) {
    case AccessLevel::Group: {
      return mode & S_IWGRP;
    }
    case AccessLevel::Other: {
      return mode & S_IWOTH;
    }
    case AccessLevel::User: {
      return mode & S_IWUSR;
    }
    }
  }
  }
}

inline bool CheckUserGroupAccess(const Perm &perm, const AccessData &accessData) {
  return ((getuid() == accessData.UID) && CheckPerm(perm, AccessLevel::User, accessData.Mode))
         || (group_member(accessData.GID) && CheckPerm(perm, AccessLevel::Group, accessData.Mode))
         || CheckPerm(perm, AccessLevel::Other, accessData.Mode);
}

inline int TranslateVFSErrorCode(const VFSErrorCode& errorCode) {
  switch (errorCode) {
  case VFSErrorCode::AccessDenied:
    return -EACCES;
  case VFSErrorCode::FileNotFound:
    return -ENOENT;
  case VFSErrorCode::DirectoryNotFound:
    return -ENOTDIR;
  case VFSErrorCode::Exception:
    return -EIO;
  case VFSErrorCode::Success:
    return 0;
  case VFSErrorCode::OSErrorCode:
    return -errno;
  }
}
}

#define VFS_ACCESS_CHECKER [](const SString& path, const bool& directory, AccessData accessData, const OpenData& openData, const bool& created)->bool {\
  return true;\
}

#define VFS_CREATOR [](const SString& sourcePath, const bool& directory, AccessData accessData, const OpenData& openData, int& handle)->bool {\
  handle = open(&sourcePath[0], openData.Flags, accessData.Mode);\
  return handle != VFSInvalidHandle;\
}

#define VFS_CLOSER [](int& handle)->bool {\
  close(handle);\
  handle = VFSInvalidHandle;\
}
#endif

typedef CVFSItem<OSHandle, AccessData, OpenData> VFSItem;
typedef CSQLiteItemStore<VFSItem> VFSItemStore;
typedef CVirtualFileSystem<VFSItemStore> VirtualFileSystem;

NS_END(2)
#endif //_SIAVFS_H
