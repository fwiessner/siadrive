#ifndef SIADRIVE_SIATRANSFERMANAGER_H
#define SIADRIVE_SIATRANSFERMANAGER_H

#include <siacommon.h>
#include <autothread.h>
#include <SQLiteCpp/Database.h>
#include <siaapi.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

class SIADRIVE_EXPORTABLE CSiaTransferManager :
  public CAutoThread,
  public virtual ITransferManager {
  public:
    enum class UploadStatus {
        Queued,
        Retry,
        Uploading
    };

  public:
    CSiaTransferManager(std::shared_ptr<CSiaApi> siaApi);

  public:
    virtual ~CSiaTransferManager();

  private:
    std::shared_ptr<CSiaApi> _siaApi;
    SQLite::Database _database;
    bool _isActive = false;
    std::mutex _dbMutex;
    std::mutex _startStopMutex;

  private:
    bool ProcessQueuedByStatus(const UploadStatus& uploadStatus, std::int32_t& activeCount);
    std::int32_t ProcessUploadsByStatus(const UploadStatus& uploadStatus, CSiaFileTreePtr fileTree);
    int RemoveFromDatabase(const SString& path);
    void SetUploadStatus(const SString& path, const UploadStatus& uploadStatus);

  protected:
    void AutoThreadCallback(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) override ;

  public:
    bool Download(const SString &path, const SString& destPath) override;
    bool Remove(const SString &path) override;
    void Start() override;
    void Stop() override;
    bool Upload(const SString &path, const SString &sourcePath) override;
};
NS_END(2)

#endif //SIADRIVE_SIATRANSFERMANAGER_H
