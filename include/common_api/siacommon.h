#ifndef _SIACOMMON_H
#define _SIACOMMON_H
#include <cstdint>
#include <regex>

#define NON_COPYABLE_ASSIGNABLE(C)\
    C(const C&) = delete;\
    C(C&&) = delete;\
    C& operator=(const C&) = delete;\
    C& operator=(C&&) = delete;
#ifdef _WIN32
#define SIAD_EXECUTABLE "siad.exe"
// Disable DLL-interface warnings
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)

// _WIN32_WINNT version constants
#define _WIN32_WINNT_WIN7                   0x0601 // Windows 7
#define _WIN32_WINNT_WIN8                   0x0602 // Windows 8
#define _WIN32_WINNT_WINBLUE                0x0603 // Windows 8.1
#define _WIN32_WINNT_WINTHRESHOLD           0x0A00 // Windows 10
#define _WIN32_WINNT_WIN10                  0x0A00 // Windows 10

// Windows 7 or above supported
#define WINVER _WIN32_WINNT_WIN7
#define _WIN32_WINNT _WIN32_WINNT_WIN7
#define _WINSOCKAPI_
#include <Windows.h>
#include <Shlwapi.h>

// Import or export functions and/or classes
#ifdef SIADRIVE_EXPORT_SYMBOLS
#define SIADRIVE_EXPORTABLE __declspec(dllexport)
#else
#define SIADRIVE_EXPORTABLE __declspec(dllimport)
#endif //SIADRIVE_EXPORT_SYMBOLS
#else
#define SIADRIVE_EXPORTABLE
#define BOOL bool
#define TRUE true
#define FALSE false
#define DWORD std::uint32_t
#define BYTE std::uint8_t

#define SIAD_EXECUTABLE "siad"
#endif
#include <memory>
#include <json.hpp>
#include <ttmath/ttmath.h>
#include <sstring.h>
#include <thread>
#include <mutex>
#include <unordered_map>
#include <deque>

#ifdef SIADRIVE_UI
#include <include/cef_app.h>

#endif
using json = nlohmann::json;

SIADRIVE_EXPORTABLE extern const char *SIADRIVE_VERSION_STRING;
SIADRIVE_EXPORTABLE extern const char *COMPAT_SIAD_VERSION;

#define TRANSACTION_FEE 0.75
#define NS_BEGIN(n) namespace n {
#define NS_END1() }
#define NS_END2() NS_END1() }
#define NS_END3() NS_END2() }
#define NS_END4() NS_END3() }
#define NS_END5() NS_END4() }
#define NS_END(c) NS_END##c()
#define COMMA ,

#define INTERFACE_CLASS_COMMON(c)\
public:\
  virtual ~c() = 0;\
protected: \
  c() {}\
public:\
  c(const c& c) = delete;\
  c(c&& c) = delete;\
  c& operator=(const c& c) = delete;\
  c& operator=(c&& c) = delete;

#define INTERFACE_CLASS_BEGIN(c) class c {\
  INTERFACE_CLASS_COMMON(c)

#define INTERFACE_CLASS_END(c) }; inline c::~c() { }
#define CONST_METHOD(m) public: virtual m const = 0;
#define METHOD(m) public: virtual m = 0;
#define PMETHOD(m) protected: virtual m = 0;

NS_BEGIN(Sia)
NS_BEGIN(Api)
class FilePath;
class CSiaDriveConfig;
class CSiaApi;
class StartupException :
  public std::exception {
  public:
    StartupException(const SString &reason) {
    }

  private:
    const SString _reason;
  public:
    virtual const char *what() const noexcept override {
      return SString::ToUtf8(_reason).c_str();
    }
};

typedef ttmath::UInt<64> Hastings;
typedef ttmath::Big<1, 30> SiaCurrency;
const std::uint8_t SIA_BLOCK_TIME_MINS = 10;
const std::uint32_t MINUTES_PER_MONTH = (730 * 60);
const std::uint32_t SIA_BLOCKS_PER_MONTH = MINUTES_PER_MONTH / SIA_BLOCK_TIME_MINS;
const std::uint32_t SIA_DEFAULT_HOST_COUNT = 50;
const SiaCurrency SIA_DEFAULT_MINIMUM_FUNDS = 4000;
const std::uint32_t SIA_DEFAULT_CONTRACT_LENGTH = SIA_BLOCKS_PER_MONTH * 3;
const std::uint32_t SIA_DEFAULT_RENEW_WINDOW = SIA_DEFAULT_CONTRACT_LENGTH / 3;

#define DEFAULT_CONFIG_FILE_PATH "~/siadrive/siadriveconfig.json"
#define DEFAULT_RENTER_DB_FILE_PATH "~/siadrive/renter_upload.db3"
#define DEFAULT_VFS_DB_FILE_PATH "~/siadrive/vfs.db3"
#define DEFAULT_RENTER_TRANSFER_DB_FILE_PATH "~/siadrive/renter_transfer.db3"

#define Property(type, name, get_access, set_access) \
private:\
type _##name;\
get_access:\
const type& Get##name() const { return _##name;}\
set_access:\
const type& Set##name(const type& value) { _##name = value; return _##name; }

#define JProperty(type, name, get_access, set_access, json_doc) \
get_access:\
type Get##name() const { return json_doc[#name].get<type>();}\
set_access:\
type Set##name(const type& value) { json_doc[#name] = value; return value; }\
protected:\
bool Check##name() { return json_doc.find(#name) != json_doc.end(); }
#define JPropertyCb(type, name, get_access, set_access, json_doc, cb) \
get_access:\
type Get##name() const { return json_doc[#name].get<type>();}\
set_access:\
type Set##name(const type& value) { if ((Check##name() && (value != json_doc[#name].get<type>())) || not Check##name()) cb(value); json_doc[#name] = value; return value; }\
protected:\
bool Check##name() { return json_doc.find(#name) != json_doc.end(); }

template<typename T>
class CSiaError {
  public:
    CSiaError() {
      SetCode(T::Success);
      SetReason("Success");
    }

    CSiaError(const CSiaError &c) {
      SetCode(c.GetCode());
      SetReason(c.GetReason());
    }

    CSiaError(CSiaError &&c) noexcept {
      _Code = std::move(c._Code);
      _Reason = std::move(c._Reason);
    }

    CSiaError(const T &t) {
      SetCode(t);
      SetReason(SString::FromUInt8((std::uint8_t) t));
    }

    CSiaError(const T &code, const SString &reason) {
      SetCode(code);
      SetReason(reason);
    }

    CSiaError &operator=(const CSiaError &c) {
      if (this != &c) {
        this->_Code = c.GetCode();
        this->_Reason = c.GetReason();
      }
      return *this;
    }

    CSiaError &operator=(CSiaError &&c) noexcept {
      if (this != &c) {
        this->_Code = std::move(c._Code);
        this->_Reason = std::move(c._Reason);
      }
      return *this;
    }

  public:
    Property(T, Code, public, private)

    Property(SString, Reason, public, private)

  public:
    operator bool() {
      return GetCode() == T::Success;
    }

    operator bool() const {
      return GetCode() == T::Success;
    }

    CSiaError &operator=(const T &code) {
      SetCode(code);
      SetReason(SString::FromUInt8((std::uint8_t) code));
      return *this;
    }
};

typedef struct {
  SString HostName;
  std::uint16_t HostPort;
  SString RequiredVersion;
  long TimeoutMs;
} SiaHostConfig;

enum class SiaCommErrorCode {
    HttpError,
    InvalidRequiredVersion,
    NoResponse,
    ServerVersionMismatch,
    Success,
    Timeout,
    UnknownFailure
};

typedef CSiaError<SiaCommErrorCode> SiaCommError;
typedef std::unordered_map<SString, SString> HttpParameters;

INTERFACE_CLASS_BEGIN(ITransferManager)
  METHOD(bool Download(const SString &path, const SString& destPath))
  METHOD(bool Remove(const SString &path))
  METHOD(void Start())
  METHOD(void Stop())
  METHOD(bool Upload(const SString &path, const SString &filePath))
INTERFACE_CLASS_END(ITransferManager)

INTERFACE_CLASS_BEGIN(ISiaApiCommunicator)
  Property(SiaHostConfig, HostConfig, public, public)
  CONST_METHOD(SString GetServerVersion());
  CONST_METHOD(SiaCommError Get(const SString &path, json &result));
  CONST_METHOD(SiaCommError Get(const SString &path, const HttpParameters &parameters, json &result));
  CONST_METHOD(SiaCommError Post(const SString &path, const HttpParameters &parameters, json &response));
INTERFACE_CLASS_END(ISiaApiCommunicator)

INTERFACE_CLASS_BEGIN(ISiaDrive)
  METHOD(void ClearCache());
  CONST_METHOD(bool IsMounted());
  METHOD(void Mount(const std::vector<std::string>& args));
  METHOD(void NotifyOnline());
  METHOD(void NotifyOffline());
  METHOD(void Unmount(const bool &clearCache = false));
  METHOD(void Shutdown());
INTERFACE_CLASS_END(ISiaDrive)

#ifdef SIADRIVE_UI
#include "include/cef_app.h"

INTERFACE_CLASS_BEGIN(ISystemTray)
  Property(std::function<void()>, TrayCloseCallback, public, public)

  CONST_METHOD(bool AllowForceClose());
  METHOD(void CheckState(CefWindowHandle hwnd));
  METHOD(void NotifyVisible(const bool &visible));
  METHOD(void Initialize(CefWindowHandle handle));
  METHOD(void Shutdown());
INTERFACE_CLASS_END(ISystemTray)

#endif

template<typename T>
inline bool ApiSuccess(const CSiaError<T> &t) {
  return t.GetCode() == T::Success;
}

inline static SiaCurrency HastingsStringToSiaCurrency(const SString &value) {
  ttmath::Parser<SiaCurrency> parser;
  parser.Parse((value + " / (10 ^ 24)").str());
  return parser.stack[0].value;
}

inline static SString SiaCurrencyToString(const SiaCurrency &value) {
  ttmath::Conv conv;
  conv.base = 10;
  conv.round = 8;
  return value.ToWString(conv);
}

inline static SString SiaCurrencyToHastingsString(const SiaCurrency &value) {
  ttmath::Parser<SiaCurrency> parser;
  parser.Parse(value.ToString() + " * (10 ^ 24)");
  ttmath::Conv conv;
  conv.scient_from = 256;
  conv.base = 10;
  conv.round = 0;
  SString ret = parser.stack[0].value.ToWString(conv);
  return ret;
}

inline static Hastings SiaCurrencyToHastings(const SiaCurrency &currency) {
  ttmath::Parser<SiaCurrency> parser;
  parser.Parse(currency.ToString() + " * (10 ^ 24)");
  ttmath::Conv conv;
  conv.scient_from = 256;
  conv.base = 10;
  conv.round = 0;
  return parser.stack[0].value.ToString(conv);
}

SString SIADRIVE_EXPORTABLE BytesToFriendlyDisplay(const SiaCurrency &bytes);
BOOL SIADRIVE_EXPORTABLE RetryAction(std::function<BOOL()> func, std::uint16_t retryCount, const DWORD &retryDelay);
SString SIADRIVE_EXPORTABLE GenerateSha256(const SString &str);

#ifdef _WIN32
std::vector<SString> SIADRIVE_EXPORTABLE GetAvailableDrives();
std::int32_t SIADRIVE_EXPORTABLE GetRegistry(const SString &name, const std::int32_t &defaultValue, const bool &user = false);
SString SIADRIVE_EXPORTABLE GetRegistry(const SString &name, const SString &defaultValue, const bool &user = false);
void SIADRIVE_EXPORTABLE SetRegistry(const SString &name, const SString &value);

class SIADRIVE_EXPORTABLE ComInitWrapper {
  public:
    ComInitWrapper();
    ~ComInitWrapper();

  private:
    const BOOL _uninit;
};
#endif
#define RetryableAction(exec, count, delayMs) RetryAction([&]()->BOOL{return exec;}, count, delayMs)
#define DEFAULT_RETRY_COUNT 10
#define DEFAULT_RETRY_DELAY_MS 1000
NS_END(2)
#endif //_SIACOMMON_H
