
#ifndef _VIRTUALFILESYSTEM_H
#define _VIRTUALFILESYSTEM_H

#include <siacommon.h>
#include <filepath.h>
#include <eventsystem.h>
#include <vfs/ivfsitem.h>
#include <vfs/vfsevents.h>
#include <vfs/vfscommon.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

template<typename Storage>
class CVirtualFileSystem {
  public:
    typedef std::function<bool(const SString &path, const bool &directory, const typename Storage::AccessData& accessData, const typename Storage::OpenData &openData, const bool& created)> AccessChecker;
    typedef std::function<bool(const SString &sourcePath, const bool &directory, const typename Storage::AccessData& accessData, const typename Storage::OpenData &openData, typename Storage::Handle &handle)> FileCreator;
    typedef std::function<bool(typename Storage::Handle &handle)> FileCloser;
    typedef IVFSItem<typename Storage::Handle, typename Storage::AccessData, typename Storage::OpenData> VFSItem;
    typedef std::shared_ptr<VFSItem> VFSItemPtr;
    typedef typename Storage::ItemInfo VFSItemInfo;
    typedef std::shared_ptr<VFSItemInfo> VFSItemInfoPtr;

  public:
    template<typename... Args>
    explicit CVirtualFileSystem(ITransferManager* transferManager, const SString &cacheLocation, AccessChecker accessChecker, FileCreator fileCreator, FileCloser fileCloser, Args &&... args) :
      _transferManager(transferManager),
      _cacheLocation(cacheLocation),
      _fileCreator(fileCreator),
      _fileCloser(fileCloser),
      _storage(accessChecker, [this](typename Storage::StorageItem *item) -> bool {
        return this->CloseCallback(item);
      }, [this](typename Storage::StorageItem *item) {
        this->ModifiedCallback(item);
      }, [this](typename Storage::StorageItem *item) {
        return this->CacheRequester(item);
      }, std::forward<Args>(args)...) {
      FilePath(cacheLocation).MakeAbsolute().MakeDirectory();
    }

  private:
    ITransferManager* _transferManager;
    const FilePath _cacheLocation;
    FileCreator _fileCreator;
    FileCloser _fileCloser;
    Storage _storage;

  private:
    inline bool CacheRequester(typename Storage::StorageItem *item) {
      return _transferManager->Download(item->GetPath(), item->GetSourcePath());
    }

    bool CloseCallback(typename Storage::StorageItem *item) {
      bool closed = false;
      if ((item->GetHandle() != VFSInvalidHandle) && (GetOpenCount(item->GetPath()) == 0)) {
        CEventSystem::EventSystem.NotifyEvent(CreateFileClosedEvent(item->GetPath(), item->GetSourcePath(), (item->GetStatus() == VFSItemStatus::Modified)));
        auto handle = item->GetHandle();
        _fileCloser(handle);
        item->SetHandle(handle);
        if (item->IsDirectory()) {
          item->SetStatus(VFSItemStatus::Created);
        } else if (item->GetStatus() == VFSItemStatus::Modified) {
          if (_transferManager->Upload(item->GetPath(), item->GetSourcePath())) {
            item->SetStatus(VFSItemStatus::Created);
          } else {
            CEventSystem::EventSystem.NotifyEvent(CreateVFSErrorEvent(__FUNCTION__, "Failed to upload: " + item->GetPath()));
          }
        }
        closed = true;
      }
      return closed;
    }

    inline SString CreateSourcePath(const SString &path) {
      return FilePath(_cacheLocation, GenerateSha256(path)).MakeAbsolute();
    }

    void ModifiedCallback(typename Storage::StorageItem *item) {
      if (item->IsDirectory()) {
        item->SetStatus(VFSItemStatus::Created);
        item->SetSize(0);
      } else {
        if (_transferManager->Remove(item->GetPath())) {
          item->SetStatus(VFSItemStatus::Modified);
          item->SetSize(FilePath::FileSize(item->GetSourcePath()));
        } else {
          CEventSystem::EventSystem.NotifyEvent(CreateVFSErrorEvent(__FUNCTION__, "Failed to remove upload: " + item->GetPath()));
        }
      }
    }

  public:
    inline bool GetAccessData(const SString& path, typename Storage::AccessData& accessData) {
      return _storage.GetAccessData(path, accessData);
    }

    inline bool GetItemInfo(const SString &path, VFSItemInfoPtr& itemInfo) {
      return _storage.GetItemInfo(path, itemInfo);
    }

    inline std::uint32_t GetOpenCount(const SString &path) {
      return _storage.GetOpenCount(path);
    }

    inline bool GetOpenItem(const std::uint64_t& id, VFSItemPtr& item) {
      return _storage.GetOpenItem(id, item);
    }

    inline bool GetUpgradeRequired() const {
      return _storage.GetUpgradeRequired();
    }

    inline bool IsDirectory(const SString& path) {
      return _storage.IsDirectory(path);
    }

    inline bool IsFileAllowed(const SString& path) {
#ifdef _WIN32
      return true;
#else
      const static std::regex r("^(\\.Trash\\-\\d+\\/[\\s\\S]*)|(\\.Trashes\\/[\\s\\S]*)$");
      return not std::regex_match(&path[0], r);
#endif
    }

    inline VFSErrorCode Create(const SString &path, const bool &directory, typename Storage::AccessData accessData, const typename Storage::OpenData &openData, VFSItemPtr &item) {
      return _storage.Create(path, CreateSourcePath(path), directory, accessData, openData, item);
    }

    inline VFSErrorCode Open(const SString &path, const bool &directory, const typename Storage::OpenData &openData, VFSItemPtr &item) {
      return _storage.Open(path, directory, openData, item);
    }

    inline VFSErrorCode OpenOrCreate(const SString &path, const bool &directory, const typename Storage::AccessData& accessData, const typename Storage::OpenData &openData, VFSItemPtr &item) {
      return _storage.OpenOrCreate(path, CreateSourcePath(path), directory, accessData, openData, item);
    }

    inline bool SetAccessData(const SString& path, const typename Storage::AccessData& accessData) {
      return _storage.SetAccessData(path, accessData);
    }

    inline void Upgrade() {
      _storage.Upgrade();
    }
};
NS_END(2)

#endif