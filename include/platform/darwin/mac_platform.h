#ifndef _MAC_PLATFORM_H
#define _MAC_PLATFORM_H
#ifdef __APPLE__
#include <string>

std::string getResourcesDir();
std::string getResourcesUrl();
bool processIsRunning(const std::string& process);
bool processIsRunningFullPath(const std::string& process);
std::string getProtectedData(const std::string& key);
void setProtectedData(const std::string& key, const std::string& value);

#endif
#endif
