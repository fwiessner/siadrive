#include "unittestcommon.h"
#include "siaapi_fixture.h"

TEST_F(SiaApiTest, ServerVersionIsEmptyWhenOffline) {
  EXPECT_CALL(*mockComm, GetServerVersion()).WillOnce(Return(SString("")));

  auto version = siaApi->GetServerVersion();
  EXPECT_TRUE(version.IsEmpty());
}

TEST_F(SiaApiTest, ServerVersionIsReturnedWhenOnline) {
  EXPECT_CALL(*mockComm, GetServerVersion()).WillOnce(Return(SString(COMPAT_SIAD_VERSION)));

  auto version = siaApi->GetServerVersion();
  EXPECT_STREQ(SString(COMPAT_SIAD_VERSION).str().c_str(), &version[0]);
}

TEST_F(SiaApiTest, AsyncServerVersionIsEmptyByDefault) {
  auto version = siaApi->GetServerVersionAsync();
  EXPECT_TRUE(version.IsEmpty());
}

TEST_F(SiaApiTest, AsyncServerVersionIsEmptyWhenOffline) {
  EXPECT_CALL(*mockComm, GetServerVersion()).WillOnce(Return(SString("")));

  siaApi->Refresh();
  auto version = siaApi->GetServerVersionAsync();
  EXPECT_TRUE(version.IsEmpty());
}

TEST_F(SiaApiTest, AsyncServerVersionIsReturnedWhenOnline) {
  EXPECT_CALL(*mockComm, GetServerVersion()).WillOnce(Return(SString(COMPAT_SIAD_VERSION)));

  siaApi->Refresh();
  auto version = siaApi->GetServerVersionAsync();
  EXPECT_STREQ(SString(COMPAT_SIAD_VERSION).str().c_str(), &version[0]);
}

TEST(SiaApi, FormatSiaPathWithLeadingDot) {
  const SString p = CSiaApi::FormatToSiaPath(".Test");
  EXPECT_STREQ("/.Test", &p[0]);
}

TEST(SiaApi, FormatSiaPathWithoutForwardSlash) {
  const SString p = CSiaApi::FormatToSiaPath("Test");
  EXPECT_STREQ("/Test", &p[0]);
}

TEST(SiaApi, FormatSiaPathWithMultiplForwardSlash) {
  const SString p = CSiaApi::FormatToSiaPath("///Test//////Two");
  EXPECT_STREQ("/Test/Two", &p[0]);
}

TEST(SiaApi, FormatSiaPathWithBackslash) {
  const SString p = CSiaApi::FormatToSiaPath("\\Test\\Two");
  EXPECT_STREQ("/Test/Two", &p[0]);
}