// Unicode for Win32
#include "unittestcommon.h"

int main(int argc, char **argv) {
  InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}