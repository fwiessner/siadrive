#include <unittestcommon.h>
#include <mockcomm.h>

const std::vector<std::string> KnownSettings = {
  "ApiPort",
  "CacheFolder",
  "DownloadTimeoutSeconds",
  "EnableDriveEvents",
  "EnableMaximumCacheSize",
  "EventLevel",
  "HostNameOrIp",
  "HostPort",
  "LaunchBundledSiad",
  "StopBundledSiad",
  "LockWalletOnExit",
  "LogDirectory",
  "MaxUploadCount",
  "MaximumCacheSize",
  "MinimumRedundancy",
  "RpcPort",
  "SiaCommTimeoutMs",
  "StoreUnlockPassword",
  "TempFolder",
	"EvictionDelay",
	"Renter_TransferDbFilePath",
	"VFS_SQLiteDbFilePath"
};

TEST(SiaDriveConfig, KnownSettingsMatchAfterCreation) {
	FilePath filePath("./test_cfg.json");
  ASSERT_TRUE(filePath.DeleteAsFile());

	auto mockComm = std::make_shared<MockSiaComm>();
	CSiaDriveConfig sdc(true, &filePath[0], mockComm);
	std::ifstream myfile(&filePath[0]);
	if (myfile.is_open()) {
		std::stringstream ss;
		ss << myfile.rdbuf();
    myfile.close();

		std::string jsonTxt = ss.str();
    ASSERT_FALSE(jsonTxt.empty());
		auto doc = json::parse(jsonTxt.c_str());

    ASSERT_EQ(KnownSettings.size(), doc.size());

    for (const auto& name : KnownSettings) {
      doc.erase(name);
    }

    ASSERT_EQ(0, doc.size());

		filePath.DeleteAsFile();
	}
}