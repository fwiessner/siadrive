#ifndef _UNITTESTCOMMON_H
#define _UNITTESTCOMMON_H

// Disable DLL-interface warnings
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <siaapi.h>
#include <siadriveconfig.h>
#include <filepath.h>
#include <fstream>

using ::testing::_;
using namespace ::testing;
using namespace Sia::Api;

static json ReadJson(const FilePath &filePath) {
  json ret;
  std::ifstream myfile(&filePath[0]);
  if (myfile.is_open()) {
    std::stringstream ss;
    ss << myfile.rdbuf();
    std::string jsonTxt = ss.str();
    if (jsonTxt.empty()) {
      myfile.close();
    } else {
      ret = json::parse(jsonTxt.c_str());
      myfile.close();
    }
  }

  return ret;
}

MATCHER_P(SStrEq, expected, "") {
  return arg == expected;
}

#endif