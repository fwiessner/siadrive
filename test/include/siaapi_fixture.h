#ifndef SIADRIVE_SIAAPI_FIXTURE_H
#define SIADRIVE_SIAAPI_FIXTURE_H

#include "unittestcommon.h"
#include "mockcomm.h"

NS_BEGIN(Sia) NS_BEGIN(Api)

class SiaApiTest :
  public ::testing::Test {
  protected:
    std::shared_ptr<MockSiaComm> mockComm;
    std::shared_ptr<CSiaApi> siaApi;

  protected:
    virtual void SetUp() override {
      mockComm.reset(new MockSiaComm());
      siaApi.reset(new CSiaApi(std::make_shared<CSiaDriveConfig>(false, "./test.json", mockComm), true));
    }

    virtual void TearDown() override {
      siaApi.reset();
      mockComm.reset();
    }
};

NS_END(2)
#endif //SIADRIVE_SIAAPI_FIXTURE_H
